FROM debian:bookworm

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get install --yes \
      composer \
      php-cli \
      php-mysql \
      php-pdo \
      php-gd \
      php-dom \
      php-curl \
      php-bcmath \
      php-xdebug \
 && apt-get clean

# Setup xdebug
RUN echo "xdebug.mode=debug,develop" >> /etc/php/8.2/mods-available/xdebug.ini
RUN echo "xdebug.client_host=host.docker.internal" >> /etc/php/8.2/mods-available/xdebug.ini
RUN echo "xdebug.start_with_request=yes" >> /etc/php/8.2/mods-available/xdebug.ini
RUN echo "xdebug.client_port=9003" >> /etc/php/8.2/mods-available/xdebug.ini
RUN echo "xdebug.idekey=PHPSTORM" >> /etc/php/8.2/mods-available/xdebug.ini

COPY ./entrypoint.sh /entrypoint.sh

WORKDIR /var/www/html

EXPOSE 8000/tcp

ENTRYPOINT [ "/entrypoint.sh" ]
