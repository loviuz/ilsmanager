<?php

namespace Tests\Unit;

use App\AccountRow;
use App\Account;
use App\Config;
use Tests\TestCase;

class AccountRowTest extends TestCase
{
    /**
     * @test
     */
    public function amounts_in_out(): void
    {
        $ar = new AccountRow;
        $ar->amount = 100.00;
        $this->assertEquals(number_format(100, 2), number_format($ar->amount_in, 2), 'Account Row in amount');
        $this->assertEquals(number_format(0, 2), number_format($ar->amount_out, 2), 'Account Row out amount');
    }

    /**
     * @test
     */
    public function account_about_membership_fee(): void
    {
        $ar = new AccountRow;
        $this->assertEquals($ar->hasAccountAboutMembershipFee(), false, 'AccountRow without Account');

        $ar = new AccountRow;
        $ar->account = new Account;
        $this->assertEquals($ar->hasAccountAboutMembershipFee(), false, 'AccountRow with non-fee Account');

        $ar = new AccountRow;
        $ar->account = \App\Config::feesAccount();
        $this->assertEquals($ar->hasAccountAboutMembershipFee(), true, 'AccountRow with fee Account');
    }
}
