{
    "Timestamp": "2024-07-01T19:23:14Z",
    "Timezone": "GMT",
    "Type": "Transfer",
    "Payer": null,
    "PayerDisplayName": "Bank Account",
    "TransactionID": "ASDLULTEST01",
    "Status": "Completed",
    "GrossAmount": {
        "currencyID": "EUR",
        "value": "-10000.00"
    },
    "FeeAmount": {
        "currencyID": "EUR",
        "value": "0.00"
    },
    "NetAmount": {
        "currencyID": "EUR",
        "value": "-10000.00"
    }
}
~~~~~~ ↑ RAW TRANSACTION ↓ TRANSACTION DETAILS ~~~~~~
{
    "Timestamp": "2024-07-01T19:23:14Z",
    "Ack": "Success",
    "CorrelationID": "fefefe123",
    "Errors": null,
    "Version": "204.0",
    "Build": "58515389",
    "PaymentTransactionDetails": {
        "ReceiverInfo": {
            "Business": null,
            "Receiver": null,
            "ReceiverID": null
        },
        "PayerInfo": {
            "Payer": null,
            "PayerID": null,
            "PayerStatus": "verified",
            "PayerName": {
                "Salutation": null,
                "FirstName": null,
                "MiddleName": null,
                "LastName": null,
                "Suffix": null
            },
            "PayerCountry": null,
            "PayerBusiness": null,
            "Address": {
                "Name": null,
                "Street1": null,
                "Street2": null,
                "CityName": null,
                "StateOrProvince": null,
                "Country": null,
                "CountryName": null,
                "Phone": null,
                "PostalCode": null,
                "AddressID": null,
                "AddressOwner": "PayPal",
                "ExternalAddressID": null,
                "InternationalName": null,
                "InternationalStateAndCity": null,
                "InternationalStreet": null,
                "AddressStatus": "Unconfirmed",
                "AddressNormalizationStatus": null
            },
            "ContactPhone": null,
            "WalletItems": null,
            "TaxIdDetails": null,
            "EnhancedPayerInfo": null
        },
        "TPLReferenceID": null,
        "PaymentInfo": {
            "TransactionID": null,
            "EbayTransactionID": null,
            "ParentTransactionID": null,
            "ReceiptID": null,
            "TransactionType": "none",
            "PaymentType": "none",
            "RefundSourceCodeType": null,
            "ExpectedeCheckClearDate": null,
            "PaymentDate": null,
            "GrossAmount": null,
            "FeeAmount": null,
            "FinancingFeeAmount": null,
            "FinancingTotalCost": null,
            "FinancingMonthlyPayment": null,
            "FinancingTerm": null,
            "IsFinancing": null,
            "SettleAmount": null,
            "TaxAmount": null,
            "ExchangeRate": null,
            "PaymentStatus": "None",
            "PendingReason": "none",
            "ReasonCode": "none",
            "HoldDecision": null,
            "ShippingMethod": null,
            "ProtectionEligibility": null,
            "ProtectionEligibilityType": null,
            "ReceiptReferenceNumber": null,
            "POSTransactionType": null,
            "ShipAmount": null,
            "ShipHandleAmount": null,
            "ShipDiscount": null,
            "InsuranceAmount": null,
            "Subject": null,
            "StoreID": null,
            "TerminalID": null,
            "SellerDetails": null,
            "PaymentRequestID": null,
            "FMFDetails": null,
            "EnhancedPaymentInfo": null,
            "PaymentError": null,
            "InstrumentDetails": null,
            "OfferDetails": null,
            "BinEligibility": null,
            "SoftDescriptor": null,
            "SoftDescriptorCity": null
        },
        "PaymentItemInfo": {
            "InvoiceID": null,
            "Custom": null,
            "Memo": null,
            "SalesTax": null,
            "PaymentItem": [
                {
                    "EbayItemTxnId": null,
                    "Name": null,
                    "Number": null,
                    "Quantity": null,
                    "SalesTax": null,
                    "ShippingAmount": null,
                    "HandlingAmount": null,
                    "InvoiceItemDetails": null,
                    "CouponID": null,
                    "CouponAmount": null,
                    "CouponAmountCurrency": null,
                    "LoyaltyCardDiscountAmount": null,
                    "LoyaltyCardDiscountCurrency": null,
                    "Amount": null,
                    "Options": null
                }
            ],
            "Subscription": {
                "SubscriptionID": null,
                "SubscriptionDate": null,
                "EffectiveDate": null,
                "RetryTime": null,
                "Username": null,
                "Password": null,
                "Recurrences": null,
                "Terms": null,
                "reattempt": "",
                "recurring": ""
            },
            "Auction": {
                "BuyerID": null,
                "ClosingDate": null,
                "multiItem": ""
            }
        },
        "OfferCouponInfo": {
            "Type": null,
            "ID": null,
            "Amount": null,
            "AmountCurrency": null
        },
        "SecondaryAddress": null,
        "UserSelectedOptions": null,
        "GiftMessage": null,
        "GiftReceipt": null,
        "GiftWrapName": null,
        "GiftWrapAmount": null,
        "BuyerEmailOptIn": null,
        "SurveyQuestion": null,
        "SurveyChoiceSelected": null
    },
    "ThreeDSecureDetails": null
}
~~~~~~ ↑ TRANSACTION DETAILS ↓ EXPECTED RESULT ~~~~~~
{
    "id": "ASDLULTEST01",
    "date": "2024-07-01T19:23:14Z",
    "amount_gross": "-10000.00",
    "amount_net": "-10000.00",
    "amount_fee": "0.00",
    "notes": "Transfer",
    "notes_xl": "Transfer"
}
