<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Default Active Theme
	|--------------------------------------------------------------------------
	|
	| Assign the default active theme to be used if one is not set during
	| runtime. This is especially useful if you're developing a very basic
	| application that does not require dynamically changing the theme.
	|
	*/

	'active' => 'ils',

	/*
	|--------------------------------------------------------------------------
	| Default Author
	|--------------------------------------------------------------------------
	|
	| Define your default author name. This is used when generating themes.
	| We will use this value in the generated theme manifest file so that
	| you may reference the author of your themes in your application.
	|
	*/

	'author' => '',

	/*
	|--------------------------------------------------------------------------
	| Default Vendor
	|--------------------------------------------------------------------------
	|
	| Define your default vendor name. This is used when generating themes.
	| We will use this value in the generated composer file so that you
	| may register your themes as a composer package as well.
	|
	*/

	'vendor' => 'vendor',

	/*
	 * CSS and JS resources.
	 * As default we assume you have access to Internet to inherit global themes.
	 * But you can host them locally.
	 */
	'jqueryurljs'     => env('JQUERY_URL_JS',     'https://www.linux.it/shared/index.php?f=jquery.js'),
	'bootstrapurljs'  => env('BOOTSTRAP_URL_JS',  'https://www.linux.it/shared/index.php?f=bootstrap.js'),
	'bootstrapurlcss' => env('BOOTSTRAP_URL_CSS', 'https://www.linux.it/shared/index.php?f=bootstrap.css'),
];
