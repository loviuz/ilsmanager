<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

use DB;
use Exception;

use App\AccountRow;
use App\Config;

/**
 * A Movement is a money transfer from (or to) your bank account(s).
 * A Movement may create AccountRow(s).
 * Each Movement may generate a single Receipt.
 */
class Movement extends Model
{

    use ILSModel;

    /**
     * Get the attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'date' => 'datetime',
    ];

    public function getEntityNameSingular()
    {
        return "Movimento";
    }

    public function bank()
    {
        return $this->belongsTo('App\Bank');
    }

    public function account_rows()
    {
        return $this->hasMany('App\AccountRow')->with('user');
    }

    public function receipt()
    {
        return $this->hasOne('App\Receipt');
    }

    public function refund()
    {
        return $this->hasMany('App\Refund');
    }

    /**
     * Scope a query to only include movements related to an Unicredit bank.
     */
    public function scopeUnicredit(Builder $query): void
    {
        $query->whereHas('bank', function ($bank_query) {
            $bank_query->unicredit();
        });
    }

    public function getAmountFormattedAttribute() {
        return format_amount( $this->amount );
    }

    public function getAmountOriginalFormattedAttribute() {
        return format_amount( $this->amount_original );
    }

    public function getAttachmentsPathAttribute()
    {
        return storage_path('accounting/' . $this->id);
    }

    public function getAttachmentsAttribute()
    {
        $ret = new Collection();

        $path = $this->attachments_path;
        if (file_exists($path)) {
            $files = scandir($path);

            foreach($files as $f) {
                if ($f[0] === '.')
                    continue;

                $ret->push($f);
            }
        }

        return $ret;
    }

    /**
     * Get the year of this Movement's date.
     *
     * @return int Year of the date
     */
    public function getDateYearAttribute()
    {
        if( !$this->date ) {
            throw new \Exception("No date, no party");
        }
        return (int) substr($this->date, 0, 4);
    }

    /**
     * Get the already-imported Movement with big margin of confidence.
     * The confidence is low on Unicredit, since there, we historically had no identifier.
     * You may want to then also run Movement#isExactlySameMovement($other) to be 100% sure.
     * @return Movement|null
     */
    public function alreadyTrackedMovement()
    {
        $query = Movement::query();

        // Skip myself if I'm already persisted.
        if (!empty($this->id))
        {
            $query = $query->where('id', '<>', $this->id);
        }

        // Base condition. Look for the copy of myself under the same bank.
        $query = $query->where('bank_id', $this->bank_id);

        // If the bank+identifier is available, use just these robust info.
        if (!empty($this->identifier))
        {
              $query = $query->where('identifier', $this->identifier);
        }
        else
        {
            // No identifier. Do desperate things.
            // Pitfall: Unicredit randomly changes "notes"...
            $query = $query->where('date', $this->date)
                           ->where('amount', $this->amount)
                           ->where('notes', $this->notes);
        }

        return $query->first();
    }

    /**
     * Check if this (non-persisted) Movement was already imported.
     * @return bool
     */
    public function alreadyTracked()
    {
        return !is_null( $this->alreadyTrackedMovement() );
    }

    /**
     * Check whenever two Movements are really the same thing.
     * This is supposed to check for everything, but the id.
     * @param Movement
     * @return bool
     */
    public function isExactlySameMovement($other)
    {
        // Limit days of difference between the two movements.
        // It happened to have a PayPal movement imported as 2024-01-09 but then moved
        // from PayPal to 2023-12-29, so, in a different year, with same identifier. lol
        $max_days_diff = 30;

        // The identifier is supposed to be unique for the whole history of the bank
        // for myself as customer.
        // This seems untrue for PayPal, that has API errors if you try to download
        // a specific Transaction ID without a date. So I guess the identifier
        // is probably unique by year :) So this is a reasonable strong check.
        return $this->bank_id    ==  $other->bank_id
            && !is_null($this->identifier)
            && $this->identifier === $other->identifier
            && $this->date->diffInDays($other->date) < $max_days_diff;
    }

    public function attachFile($file)
    {
        $file->move($this->attachments_path, $file->getClientOriginalName());
    }

    /**
     * Append some proposed AccountRow(s) covering membership fees.
     * This method tries to update already-existing AccountRow(s).
     * To recognize which of them were created, loop your "account_rows"
     * and look for ones without an "id" or with "edited".
     * @return boolean
     */
    public function proposeMembershipFeesForUser($user)
    {
        if (!is_object($user)) {
            $user_id = $user;
            $user = User::find($user_id);
            if (is_null($user)) {
                Log::error('Errore nel riconoscimento utente di cui generare le quote di iscrizione: ' . $user_id);
                return false;
            }
        }

        $fees_account = Config::feesAccount();
        $donations_account = Account::where('donations', true)->first();
        $year = $user->firstMissingFeeYear();

        // First, try expanding an already-existing stub AccountRow.
        $already_existing_ar = $this->findStubAccountRow();
        $ar = $already_existing_ar;
        if ($ar) {
            $amount = $ar->amount;
        } else {
            $amount = $this->getAccountRowsAmountDiffError();
        }

        while($amount >= $user->feeAmount) {
            // Add a new AccountRow or expand the initial one.
            if (!$ar) {
                $ar = new AccountRow;
                $ar->movement_id = $this->id;
            }
            $ar->account_id = $fees_account->id;
            $ar->amount = $user->feeAmount;
            $ar->user_id = $user->id;
            $ar->notes = AccountRow::createFeeNote($user, $year);
            if ($ar === $already_existing_ar) {
                $ar->edited = true;
            } else {
                $this->account_rows->push($ar);
            }

            $amount = bcsub($amount, $user->feeAmount, 2);
            $year++;

            // Force the creation of a new AccountRow on the next run.
            $ar = null;
        }

        if ($amount > 0) {
            if (!$ar) {
                $ar = new AccountRow;
                $ar->movement_id = $this->id;
            }
            $ar->account_id = $donations_account->id;
            $ar->amount = $amount;
            $ar->notes = sprintf('Donazione da parte di %s', $user->printable_name);
            $ar->user_id = $user->id;
            if ($ar === $already_existing_ar) {
                $ar->edited = true;
            } else {
                $this->account_rows->push($ar);
            }
        }

        return true;
    }

    /**
     * Find the most recent Account Row from the whole history, using a generic note
     *
     * @param string $guess_by_note
     * @return AccountRow
     */
    public function proposeAccountRowFromHistory($guess_by_note)
    {
        // Get the already-saved AccountRow's Account IDs.
        // This is useful since probably we wont to import what remains.
        $current_account_ids = $this->account_rows->pluck('account_id');

        $prev_movement_query = Movement::query()
          ->where('notes', 'like', $guess_by_note)
          ->whereHas('account_rows', function($query) use ($current_account_ids) {
              if ($current_account_ids) {
                  $query->whereNotIn('account_id', $current_account_ids);
              }
          })
          ->orderBy('id', 'desc');

        // Do not suggest myself.
        if ($this->id) {
            $prev_movement_query->whereNot('id', $this->id);
        }

        $prev_movement = $prev_movement_query->first();
        if ($prev_movement) {
            $prev_account_row = $prev_movement->account_rows()->first();
            if ($prev_account_row) {
                $remaining_amount = $this->getAccountRowsAmountDiffError();
                $remaining_plus_fee = bcadd($remaining_amount, $prev_account_row->amount, 2);
                if ($remaining_plus_fee > 0) {
                    $ar = new AccountRow;
                    $ar->movement_id = $this->id;
                    $ar->account_id = $prev_account_row->account_id;
                    $ar->user_id = $prev_account_row->user_id;
                    $ar->section_id = $prev_account_row->section_id;
                    $ar->notes = $prev_account_row->notes;
                    $ar->amount = $prev_account_row->amount;
                }
                return $ar;
            }
        }

        return null;
    }

    /**
     * Try to give a meaning to this Movement, from its note.
     * You can setup some regular expressions in each Bank configuration,
     * to match your Movement and propose an AccountRow.
     *
     * This method MAY modify some already-existing AccountRow(s) but
     * WITHOUT PERSISTING ANYTHING. Or, this method will just return some new
     * proposed AccountRow.
     *
     * To know what AccountRow was created or expanded, just look at what
     * AccountRow is flagged as "edited".
     *
     * @return BankRule|null Returns the matching BankRule, or null.
     */
    private function guessByBankRulePatterns()
    {
        // First, check if we have at least one incomplete AccountRow.
        // In case, this will be expanded, instead of creating a new one.
        $ar = $this->findStubAccountRow();

        $remaining_amount = $this->getAccountRowsAmountDiffError();

        foreach($this->bank->rules as $rule) {
            if (preg_match_or_throw($rule->fixed_rule, $this->notes)) {

                // Avoid to propose something (like Spese Commercialista) if you already have
                // something like that, already prefilled.
                if ($this->exists) {
                    $already_exists = $this->account_rows()
                        ->where('account_id', $rule->account_id)
                        ->first();

                    if ($already_exists) {
                        continue;
                    }
                }

                // Fill the already-existing stub AccountRow, or,
                // prepare a new fresh one.
                if ($ar) {
                    // Transient attribute indicating that this was edited
                    // and should be persisted.
                    // This is read by the accountrow.editblock view.
                    $ar->edited = true;
                } else {
                    $ar = new AccountRow;
                    $ar->movement_id = $this->id;
                }

                $ar->account_id = $rule->account_id;
                $ar->notes = $rule->notes;
                $ar->amount = $remaining_amount;

                // Be sure that this AccountRow is listed, even
                // if it may be not persisted.
                // So, we can list it in views, and save it.
                if (!$ar->id) {
                    $this->account_rows->push($ar);
                }

                return $rule;
            }
        }

        return null;
    }

    /**
     * Try to give a meaning to this Movement, from its notes.
     * First, we try looking at Bank configurations and their regular
     * expressions.
     * Then, we try looking at a well-known pattern for members fees.
     * This method ALTERS the "account_rows" datamember, so, you can list them,
     * and you can save them.
     * @return boolean True if something interesting was guessed, false otherwise.
     */
    public function guessRows()
    {
        // Skip corner cases that are surely not a single membership.
        if ($this->start_of_year) {
            return false;
        }

        // If we are lucky, it matches a well-known pattern, like well-known suppliers,
        // well-known bank fees, etc. so, nothing more should be done.
        // Note that, if this is true, probably, something already-existing MAY
        // be edited, or something new and unsaved MAY be appended in the "account_rows" field.
        if ($this->guessByBankRulePatterns()) {
            return true;
        }

        // Then, try to match membership fees.
        if ($this->bank->type === 'paypal') {
            list($name, $email, $causal) = explode(' - ', $this->notes . " -  -  - ", 3);

            // try to find from the users table using name and surname
            // @TODO: if an user on PayPal has 1 name, and on ILS Manager has 2+ names, it won't be found
            // TODO: better SQL management
            $name = str_replace("'", "\'", $name);
            $user = User::where(DB::raw("CONCAT(name, ' ', surname)"), $name)->first();
            if (!is_null($user)) {
                $this->proposeMembershipFeesForUser($user);
                return true;
            }


            // Or, try to find from the already-imported Account Rows
            $guess_note = sprintf('%s - %s - %%', $name, $email);
            $ar = $this->proposeAccountRowFromHistory($guess_note);
            if ($ar) {
                // check if this a "Saldo quota YYYY"
                $year = $ar->getFeeYear();
                if ($year && is_numeric($year)) {
                    // propose a copy of this AccountRow with year + 1
                    $ar_proposed = $ar->replicate();
                    $ar_proposed->notes = AccountRow::createFeeNote( $ar->user, $year + 1 );

                    $this->account_rows->push($ar_proposed);
                    return true;
                }
            }
        }
        else if ($this->bank->type === 'unicredit') {
            $name = trim(preg_replace('/^BONIFICO A VOSTRO FAVORE BONIFICO SEPA DA ([A-Z ]*) PER .*$/', '\1', $this->notes));
            $user = User::where(DB::raw("CONCAT(surname, ' ', name)"), $name)->first();
            if (!is_null($user)) {
                $this->proposeMembershipFeesForUser($user);
                return true;
            }

            $ar = $this->proposeAccountRowFromHistory($this->notes);
            if ($ar) {
                $this->account_rows->push($ar);
                return true;
            }
        }

        // Create a stub AccountRow with the supposed remaining amount.
        $amount = $this->getAccountRowsAmountDiffError();
        if ($amount !== '0.00') {
            $ar = new AccountRow;
            $ar->amount = $amount;
            $this->account_rows->push($ar);
        }

        return false;
    }

    /**
     * Get the AccountRows "IN" sum.
     * This is safe to be called multiple times.
     * @return string
     */
    public function getAccountRowsInSum()
    {
        $sum = '0.00';
        foreach( $this->account_rows as $ar ) {
            $sum = bcadd($sum, $ar->amount_in, 2);
        }
        return $sum;
    }

    /**
     * Get the AccountRows "IN" sum.
     * @return string
     */
    public function getAccountRowsOutSum()
    {
        $sum = '0.00';
        foreach ($this->account_rows as $ar) {
            $sum = bcadd($sum, $ar->amount_out, 2);
        }
        return $sum;
    }

    /**
     * Get the AccountRows "IN" - "OUT" sum.
     * @return string
     */
    public function getAccountRowsSum() {
        return bcsub($this->getAccountRowsInSum(), $this->getAccountRowsOutSum(), 2);
    }

    /**
     * Get the error difference between the Movement Amount and its AccountRow(s).
     * @return string
     */
    public function getAccountRowsAmountDiffError() {
        return bcsub($this->amount, $this->getAccountRowsSum(), 2);
    }

    /**
     * Check whenever the Movement amount matches its AccountRow(s).
     * @return bool
     */
    public function isAmountMatchingAccountRows() {
        $diff = abs( $this->getAccountRowsAmountDiffError() );
        return $diff < 0.009;
    }

    public function generateReceipt()
    {
        $causals = [];
        $headers = [];

        if ($this->amount <= 0) {
            return;
        }

        foreach($this->account_rows as $ar) {
            if ($ar->user) {
                $headers[] = $ar->user->full_address;
            }

            $causals[] = trim($ar->notes);
        }

        $receipt = $this->receipt;

        if (is_null($receipt)) {
            $receipt = new Receipt();
            $receipt->movement_id = $this->id;
        }

        $receipt->date = date('Y-m-d');
        $receipt->header = join("\n", $headers);
        $receipt->causal = join("\n", $causals);
        $receipt->save();
    }

    /**
     * Validate the whole Movement and its AccountRow(s).
     * An invalid Movement MAY be saved but cannot be marked as "reviewed".
     * @throws Exception
     */
    public function validate()
    {
        // First, validate children entities.
        foreach ($this->account_rows as $i => $ar)
        {
            try {
                $ar->validate();
            } catch(Exception $e) {
                throw new Exception(sprintf(
                  "Errore nel movimento contabile n. %d: %s",
                  $i + 1,
                  $e->getMessage()
                ), 0, $e);
            }
        }

        // Then, validate globally.
        if (!$this->isAmountMatchingAccountRows()) {
            throw new Exception(sprintf(
              "La somma dei movimenti interni non combacia. Il movimento ha importo %d. Quelli interni hanno somma %d. Scarto: %s %s",
              $this->amount,
              $this->getAccountRowsSum(),
              $this->getAccountRowsAmountDiffError(),
              'EUR'
            ));
        }
    }

    /**
     * Find one AccountRow that needs to be completed.
     *
     * @return AccountRow|null
     */
    public function findStubAccountRow()
    {
        foreach ($this->account_rows as $account_row) {
            if (!$account_row->account_id) {
                return $account_row;
            }
        }
        return null;
    }

    /**
     * Check whenever this Movement supports a data refresh.
     * @return bool
     */
    public function supportsRefresh()
    {
        return isset($this->id) && isset($this->bank) && $this->bank->type === 'paypal';
    }

}
