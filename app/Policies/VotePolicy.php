<?php

namespace App\Policies;

use App\User;
use App\Vote;
use Illuminate\Auth\Access\HandlesAuthorization;

class VotePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // There is not such index.
        // I've afraid that this can create problems if set to false.
        return true;
    }

    public function view(User $user, Vote $model)
    {
        return true;
    }

    public function create(User $user)
    {
        return true;
    }

    public function update(User $user, Vote $model)
    {
        return false;
    }

    public function delete(User $user, Vote $model)
    {
        return false;
    }
}
