<?php

namespace App\Exceptions;

use Exception;

class CannotOverwriteMovementIdentifierException extends Exception {};
