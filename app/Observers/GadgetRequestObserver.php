<?php

namespace App\Observers;

use Exception;

use App\Config;
use App\GadgetRequest;
use App\Mail\GadgetRequestCancelled;
use App\Mail\GadgetRequestCreated;
use App\Mail\GadgetRequestNotificationForStaff;
use App\Mail\GadgetRequestPreparing;
use App\Mail\GadgetRequestShipped;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Mail;

class GadgetRequestObserver {
    public function created(GadgetRequest $gadgetRequest) {
        // Send copy to the user who requested gadgets.
        // Do not crash if it's not possible.
        try {
            $gadgetRequest->sendMailable(new GadgetRequestCreated($gadgetRequest));
        } catch(Exception $e) {
            // Do not crash if it's not possible but log stack trace.
            report($e);
        }

        // Send realtime notification to admins, if they want this.
        // For example Ferdinando Traversa does not like this :D
        if (Config::getConfig('gadget_requests_realtime_notifications')) {
            // Get all Role IDs.
            $role_ids = Role::where('name', 'shipper' )
                ->pluck('id');

            // Get email of each user with at least one of these roles.
            if ($role_ids) {
                $notifiable_users = User::query()
                    ->where('status', 'active')
                    ->whereHas('roles', function ( $query ) use ( $role_ids ) {
                        $query->whereIn('id', $role_ids);
                    } )
                    ->get();

                $notifiable_emails = $notifiable_users->map( function( $user ) {
                    return $user->custom_email;
                } );

                if ($notifiable_emails->isNotEmpty()) {
                    try {
                        Mail::to($notifiable_emails)->send(new GadgetRequestNotificationForStaff($gadgetRequest));
                    } catch(Exception $e) {
                        // Do not crash if it's not possible but log stack trace.
                        report($e);
                    }
                }
            }
        }
    }

    public function updated(GadgetRequest $gadgetRequest) {
        if ($gadgetRequest->isDirty('status_id')) {
            // We like to keep the user informed. LOL.
            switch ($gadgetRequest->status->uid) {
                case 'preparing':
                    $gadgetRequest->sendMailable(new GadgetRequestPreparing($gadgetRequest));
                    break;
                case 'shipped':
                    $gadgetRequest->sendMailable(new GadgetRequestShipped($gadgetRequest));
                    break;
                case 'cancelled':
                    $gadgetRequest->sendMailable(new GadgetRequestCancelled($gadgetRequest));
                    break;
            }
        }
    }
}
