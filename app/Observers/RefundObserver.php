<?php

namespace App\Observers;

use Mail;
use Log;

use App\Mail\RefundRequired;

use App\Refund;
use App\Config;

class RefundObserver
{
    public function created(Refund $refund)
    {
        try {
            Mail::to(Config::getConfig('association_email'))->send(new RefundRequired($refund));
        }
        catch(\Exception $e) {
            Log::error('Impossibile inviare notifica per nuovo rimborso spese registrato: ' . $e->getMessage());
        }
    }
}
