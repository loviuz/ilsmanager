<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Refund extends Model
{

    use ILSModel;

    /**
     * Get the generic name of this entity, singular.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public function getEntityNameSingular()
    {
        return "Rimborso Spese";
    }

    /**
     * Get the object short name.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public function getObjectShortName()
    {
        // TODO: have a title
        return $this->id;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function movement()
    {
        return $this->belongsTo('App\Movement');
    }

    public function getPrintableDescriptionAttribute()
    {
        return sprintf('%s € - %s - %s', $this->amount, $this->user ? $this->user->printable_name : '?', $this->section ? 'Sezione Locale di ' . $this->section->city : 'Nessuna Sezione Locale');
    }

    public function getAttachmentsReceiptsPathAttribute()
    {
        return storage_path('refunds/' . $this->id);
    }

    public function getAttachmentsQuotesPathAttribute()
    {
        return storage_path('refunds/' . $this->id. '/quotes');
    }

    public function getAttachmentsReceiptsAttribute()
    {
        $ret = new Collection();
        $path = $this->attachments_receipts_path;
        if (file_exists($path)) {
            $files = scandir($path);
            foreach ($files as $f) {
                if (is_file($path . '/' . $f)) {
                    $ret->push($f);
                }
            }
        }

        return $ret;
    }

    /**
     * Get the RefundStatus object from this Refund, if any.
     * @return RefundStatus
     */
    public function getRefundStatusAttribute()
    {
        return RefundStatus::findByID($this->refunded);
    }

    public function getAttachmentsQuotesAttribute()
    {
        $ret = new Collection();

        $path = $this->attachments_quotes_path;
        if (file_exists($path)) {
            $files = scandir($path);
            foreach ($files as $f) {
                if (is_file($path . '/' . $f)) {
                    $ret->push($f);
                }
            }
        }

        return $ret;
    }

    public function attachReceipt($file)
    {
        $file->move($this->attachments_receipts_path, $file->getClientOriginalName());
    }

    public function attachQuote($file)
    {
        $file->move($this->attachments_quotes_path, $file->getClientOriginalName());
    }

}
