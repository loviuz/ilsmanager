<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Vote;

class Votation extends Model
{
    protected $casts = [
        'options' => 'array',
    ];

    public function assembly()
    {
        return $this->belongsTo('App\Assembly');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function hasVoted($user)
    {
        foreach($this->assembly->delegations($user) as $voter) {
            if (Vote::where('votation_id', $this->id)->where('user_id', $voter->id)->count() == 0) {
                return false;
            }
        }

        return true;
    }

    public function formatResults()
    {
        $final = [];

        foreach($this->options as $option) {
            $final[$option] = (object) [
                'option' => $option,
                'count' => 0,
            ];
        }

        foreach($this->votes as $vote) {
            foreach($vote->answer as $voted) {
                if (isset($final[$voted])) {
                    $final[$voted]->count += 1;
                }
                else {
                    Log::error('Errore in conteggio risultati votazione: ' . $voted . ' non è una opzione valida');
                }
            }
        }

        $final = array_values($final);
        usort($final, function($a, $b) {
            return $a->count <=> $b->count;
        });

        $final = array_reverse($final);

        return $final;
    }
}
