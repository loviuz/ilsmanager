<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GadgetRequestPreparing extends Mailable
{
    use Queueable, SerializesModels;

    private $gadgetRequest;

    public function __construct($gadgetRequest)
    {
        $this->gadgetRequest = $gadgetRequest;
    }

    public function build()
    {
        return $this->subject('La tua richiesta di spedizione gadget è in preparazione')
            ->view('email.gadget_request_preparing', ['gadgetRequest' => $this->gadgetRequest]);
    }
}
