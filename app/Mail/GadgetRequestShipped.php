<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GadgetRequestShipped extends Mailable
{
    use Queueable, SerializesModels;

    private $gadgetRequest;

    public function __construct($gadgetRequest)
    {
        $this->gadgetRequest = $gadgetRequest;
    }

    public function build()
    {
        return $this->subject('La tua richiesta di gadget è stata spedita')
            ->view('email.gadget_request_shipped', ['gadgetRequest' => $this->gadgetRequest]);
    }
}
