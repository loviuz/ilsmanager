<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GadgetRequestCreated extends Mailable
{
    use Queueable, SerializesModels;

    private $gadgetRequest;

    public function __construct($gadgetRequest)
    {
        $this->gadgetRequest = $gadgetRequest;
    }

    public function build()
    {
        // TODO: convert to markdown
        return $this->subject('La tua richiesta di gadget è stata ricevuta')
            ->view('email.gadget_request_created', ['gadgetRequest' => $this->gadgetRequest]);
    }
}
