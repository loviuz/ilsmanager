<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistered extends Mailable
{
    use Queueable, SerializesModels;

    private $user = null;

    private $to_role = null;

    public function __construct($user, $to_role = null)
    {
        $this->user = $user;
        $this->to_role = $to_role;
    }

    public function build()
    {
        return $this->view('email.new_user', [
            'user' => $this->user,
            'to_role' => $this->to_role,
        ] );
    }
}
