<?php

namespace App\Actions;

use App\Account;
use App\AccountRow;
use App\Exceptions\CannotOverwriteMovementIdentifierException;
use App\Movement;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use function preg_match_or_throw;

/**
 * Parse a Unicredit Movement notes to intercept some bank fees,
 * and eventually generate/update the AccountRow to describe these bank fees,
 * and eventually update the "original_amount" (what the user paid)
 * and the "amount" (what we really received) fields.
 * Before calling this, the movement->notes should be already populated.
 * This action is designed to be idempotent.
 * This method DOES NOT persist the Movement and one of the related AccountRow(s).
 * You MUST run save() your entities after calling this, on both the original
 * Movement, and your returned AccountRow.
 *
 * @author Valerio Bozzolan
 */
class MovementUnicreditRefreshParsingNotes
{
    private Movement $movement;

    private ?AccountRow $arBankFees = null;

    private bool $hasChanges = false;

    /**
     * Constructor
     *
     * @param  Movement  $movement  Movement that will be edited.
     */
    public function __construct(Movement $movement)
    {
        $this->movement = $movement;
    }

    /**
     * Parse the Movement->notes, assuming that this is an Unicredit Movement.
     * If this method returns an AccountRow, the Movement and that AccountRow
     * MUST be persisted by you.
     * You may want to enjoy the save() method, after this, if you like the result.
     */
    public function handle(): void
    {
        // Phase 1. Refresh original amount, if needed.
        $this->refreshOriginalAmountAndBankFees();

        // Pase 2. Refresh the unique identifier, if needed.
        $this->refreshIdentifier();
    }

    /**
     * Get the AccountRow that is proposed or going to be updated.
     * This object, if available, is not persisted.
     * See the commodity method save() from this class to persist changes.
     * to persist it.
     */
    public function getArBankCosts(): ?AccountRow
    {
        return $this->arBankFees;
    }

    /**
     * Get the Movement.
     * Calling this may be useful if you called pleaseExpandExistingDuplicates()
     * since, in that case, the output Movement may be different from the input
     * one.
     */
    public function getMovement(): Movement
    {
        return $this->movement;
    }

    /**
     * Check if this action caused some changes to be saved or not.
     * See the commodity method save() from this class to persist changes.
     */
    public function getHasChanges(): bool
    {
        return $this->hasChanges;
    }

    /**
     * Commodity method to persist any changes.
     * This MUST be called after the handle() method.
     * This will create a new entity.
     */
    public function save(): void
    {
        DB::beginTransaction();

        // Make sure to first save the Movement, so it has an ID later.
        $this->movement->save();

        // Eventually attach the new AccountRow / update it.
        if ($this->arBankFees) {

            if (is_null($this->arBankFees->movement_id)) {
                $this->arBankFees->movement_id = $this->movement->id;
            } elseif ($this->arBankFees->movement_id !== $this->movement->id) {
                // Replacing the Movement ID of the AccountRow sounds like a programming mistake.
                throw new Exception("Attempt to replace Movement ID of AccountRow IDs: {$this->arBankFees->id}. Before: {$this->arBankFees->movement_id}. After: {$this->movement->id}");
            }

            $this->arBankFees->save();
        }

        DB::commit();
    }

    public function pleaseExpandExistingDuplicate(): void
    {
        // First, check for a duplicate Movement, and eventually refresh that.
        $existing_movement = $this->movement->alreadyTrackedMovement();
        if (! $existing_movement || ! $existing_movement->isExactlySameMovement($this->movement)) {
            return;
        }

        Log::debug(sprintf('Ready to refresh data of already-existing Movement ID: %d that had amount: %s and date: %s and amount_original: %s',
            $existing_movement->id,
            $existing_movement->amount,
            $existing_movement->date,
            $existing_movement->amount_original));

        $existing_movement->amount = $this->movement->amount;
        $existing_movement->amount_original = $this->movement->amount_original;
        $existing_movement->date = $this->movement->date;
        $existing_movement->notes = $this->movement->notes;
        $this->movement = $existing_movement;

        // Secondly, check if we are proposing an AccountRow.
        $ar_bank_costs = $this->getArBankCosts();
        if (! $ar_bank_costs) {
            return;
        }

        // Check if our proposed AccountRow can be merged into an already-existing AccountRow that has Bank Costs.
        $existing_ar_bank_fees = $existing_movement->account_rows()->ofBankCosts()->first();
        if (! $existing_ar_bank_fees) {
            return;
        }

        Log::debug(sprintf('Ready to refresh data of already-existing AccountRow ID: %d that had amount: %s',
            $ar_bank_costs->id,
            $ar_bank_costs->amount));

        $existing_ar_bank_fees->amount = $ar_bank_costs->amount;
        $existing_ar_bank_fees->notes = $ar_bank_costs->notes;
        $this->arBankFees = $existing_ar_bank_fees;
    }

    /**
     * Parse the Unicredit notes to extract bank fees, like '0.74'.
     * The fees are extracted as-is, so, with positive sign.
     * This may generate an AccountRow, or still try to update an AccountRow.
     * The resulting AccountRow is NOT persisted.
     */
    private function refreshOriginalAmountAndBankFees(): void
    {
        // Set shortcuts.
        $prefix = $this->getLogPrefix();
        $movement = $this->movement;

        // Do nothing if the notes do not have bank fees.
        $unicredit_fees_sum = $this->parseBankFees();
        $has_bank_fees = $unicredit_fees_sum !== '0.00';
        if (! $has_bank_fees) {
            Log::debug("{$prefix}: no bank fees found from: '{$movement->notes}'");

            return;
        }

        // Originally, the fees seem to be always positive,
        // but we need a negative value. lol.
        $unicredit_fees_sum_negative = $this->toggleSign($unicredit_fees_sum);

        // Do nothing if the original amount was already populated.
        // This very probably indicates that we already executed this action,
        // and executing this again may compromise the amount.
        if (! is_null($movement->amount_original)) {
            Log::debug("{$prefix}: it already has an amount_original so the bank fees were probably already imported from the notes - will not import again");

            return;
        }

        // Check if we already have a bank fee, so we can update it,
        // instead of creating a new one, potentially duplicated.
        if ($movement->id) {
            // Find the Account that describes bank fees.

            // This query can return NULL.
            $this->arBankFees = $movement->account_rows()
                ->where('account_id', Account::bankCostsId())
                ->first();
        }

        // Prepare a new AccountRow to store the bank fee, since none was found.
        if ($this->arBankFees) {
            Log::debug("{$prefix}: proposing update of AccountRow ID {$this->arBankFees->id} with bank fees amount: {$unicredit_fees_sum_negative} taken from notes: {$movement->notes}");
        } else {
            Log::debug("{$prefix}: proposing creation of AccountRow with bank fees amount: {$unicredit_fees_sum_negative} taken from notes: {$movement->notes}");

            // Create a new AccountRow.
            // This still needs the movement->id. Will be set from the save() method in this class.
            $this->arBankFees = new AccountRow;
            $this->arBankFees->account_id = Account::bankCostsId();
            $this->arBankFees->notes = 'Commissioni Unicredit';
        }

        // Start from the amount, that is, what we really received.
        // Calculate what was really paid from the source side, before these damn bank fees.
        // Example 1: if we received an amount of 99 EUR, and if the fee written in the notes is 1 (so, -1),
        // so the original_amount is supposed to be 100.
        // Example 2: if we paid 101 (amount: -101), and if the fee written in the notes is 1 (so, -1),
        // so the original_amount is supposed to be -100.
        $original_amount_estimated = bcadd($movement->amount, $unicredit_fees_sum, 2);
        Log::debug("{$prefix}: setting original_amount: {$original_amount_estimated}. Current amount: {$movement->amount}. Bank fees found: {$unicredit_fees_sum_negative}");

        // Refresh amounts. The original was null if we are here.
        // In the Movement "amount_original" field we store what the end-user really paid.
        // In the Movement "amount" field we store what we received.
        // In the AccountRow we store the exact bank fee amount we paid (so, usually negative income).
        $movement->amount_original = $original_amount_estimated;
        $this->arBankFees->amount = $unicredit_fees_sum_negative;

        // DO NOT SAVE HERE :D allow consumer to manually call save() or not later.
        $this->hasChanges = true;
    }

    /**
     * Extract the Unicredit Transaction identifier from notes like "bla bla TNR 123".
     * This identifier is saved in the Movement object itself.
     */
    private function refreshIdentifier(): void
    {
        // Set shortcuts.
        $prefix = $this->getLogPrefix();

        // Do nothing of nothing would change.
        $old_identifier = $this->movement->identifier;
        $new_identifier = $this->parseIdentifier();
        if ($old_identifier === $new_identifier) {
            return;
        }

        // Losing the identifier is strange. Red alert. Probably it's better to do nothing.
        if ($old_identifier && ! $new_identifier) {
            Log::error("{$prefix}: already has identifier: {$old_identifier} but was not parsed from the notes. Keeping as-is but you may want do double-check.");

            return;
        }

        // When we are setting a completely different identifier,
        // scream violently, since this is probably caused by a developer error.
        if ($old_identifier) {
            throw new CannotOverwriteMovementIdentifierException("Cannot overwrite a Movement identifier. Unicredit movement with identifier '{$new_identifier}' would take new identifier '{$old_identifier}'. This is probably wrong.");
        }

        Log::debug("{$prefix}: proposing new identifier: {$new_identifier}");
        $this->movement->identifier = $new_identifier;

        $this->hasChanges = true;
    }

    /**
     * Extract bank fees from Movement's notes.
     * The result is as-is, so, usually a positive amount.
     * If in the notes you have both "SPESE" and "COMM" this returns their sum.
     *
     * @return string Number with 2 digits. So, if zero, this is always "0.00".
     */
    private function parseBankFees(): string
    {
        // Unicredit has silly notes describing their bank fees.
        // Unicredit has two kind of fees, but we just import their sum.
        // Interestingly, these fees are not shared in their QIF structured format,
        // so this is really the best way to catch these... (and it's terrible lol).
        $notes = $this->movement->notes;

        // Run regular expressions.
        // Literal examples (with 14 spaces):
        //  ' SPESE              0,01'
        //  ' COMM              0,01'
        // I think spaces may change.
        $unicredit_matcher_fees = [];
        $unicredit_matcher_comm = [];
        preg_match_or_throw('/ SPESE         +([\d]+,[\d]+) /', $notes, $unicredit_matcher_fees);
        preg_match_or_throw('/ COMM         +([\d]+,[\d]+) /', $notes, $unicredit_matcher_comm);

        // Get matched amounts.
        $unicredit_fees = parse_unicredit_number($unicredit_matcher_fees[1] ?? '0,00');
        $unicredit_comm = parse_unicredit_number($unicredit_matcher_comm[1] ?? '0,00');

        // Sum amounts in a safe way.
        $sum_fees = bcadd($unicredit_fees, $unicredit_comm, 2);

        // Consistency check.
        // This non-negative contraint is added just because it never happened and I don't
        // know what we should do in this case later.
        // Better to scream violently so we can add a unit test about this and
        // have better coverage.
        $fees_are_negative = bccomp($sum_fees, 0) === -1;
        if ($fees_are_negative) {
            $prefix = $this->getLogPrefix();
            throw new Exception("{$prefix}: found negative fees from notes: '{$this->movement->notes}'. Operation not expected. Bank costs found: {$sum_fees}");
        }

        return $sum_fees;
    }

    /**
     * Extract the Unicredit transaction ID, that is, a numeric identifier.
     * This is returned as string, since this may start with a zero,
     * and that zero would be significant I guess.
     *
     * @return string|null Movement identifier, or null if cannot be parsed.
     */
    private function parseIdentifier(): ?string
    {
        // Sometime it's "TRN 123", sometime "TRN  123".
        $matches = [];
        preg_match_or_throw('/ TRN +([\d]+)$/', $this->movement->notes, $matches);

        return $matches[1] ?? null;
    }

    /**
     * Get a prefix useful for the log.
     */
    private function getLogPrefix(): string
    {
        $here = __CLASS__;
        $id = $this->movement->id ?? 'n.a.';

        return "[{$here}] processing Unicredit Movement date: {$this->movement->date} id: {$id}";
    }

    /**
     * Toggle the sign of a number.
     */
    private function toggleSign(string $n): string
    {
        return bcmul($n, -1, 2);
    }
}
