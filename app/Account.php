<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * An Account is an internal Bank Account like "Donations" or "Fees" etc.
 * An Account may have a parent Account, for example "Goods" > "Web Services".
 */
class Account extends Model
{
    public function parent()
    {
        return $this->belongsTo('App\Account');
    }

    public function children()
    {
        return $this->hasMany('App\Account', 'parent_id');
    }

    public function getPrintableNameAttribute()
    {
        $names[] = $this->name;

        $iter = $this;
        while($parent = $iter->parent) {
            $names[] = $parent->name;
            $iter = $parent;
        }

        return join('/', array_reverse($names));
    }

    /**
     * Check whenever this Account describes Membership Fees.
     *
     * @return boolean
     */
    public function isAboutMembershipFee()
    {
        return $this->id == Config::feesAccount()->id;
    }

    /**
     * Get the Account ID for the bank costs.
     * This method has an internal cache so it's safe to be called multiple times.
     */
    public static function bankCostsId(): int
    {
        static $cache = null;
        if ($cache === null) {
            $cache = Account::where('bank_costs', true)->firstOrFail()->id;
        }
        return $cache;
    }

}
