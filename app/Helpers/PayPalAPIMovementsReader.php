<?php
/**
    ILS Manager
    Copyright (C) 2021-2024 Italian Linux Society, contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use PayPal\PayPalAPI\TransactionSearchReq;
use PayPal\PayPalAPI\TransactionSearchRequestType;
use PayPal\PayPalAPI\GetTransactionDetailsReq;
use PayPal\PayPalAPI\GetTransactionDetailsRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;

/**
 * PayPal API movements reader
 *
 * Originally from:
 *   https://github.com/ItalianLinuxSociety/Donazioni/blob/master/read_paypal.php
 *
 * @author Valerio Bozzolan, Roberto Guido
 */
class PayPalAPIMovementsReader
{
    /**
     * Process incoming PayPal transactions.
     *
     * @param array $args Array of arguments. All of them:
     *   startDate:     DateTime When to start your search (default: endDate - 6 months)
     *   endDate:       DateTime When to stop your search (default: today)
     *   transactionId: PayPal transaction ID
     *   itemCallback:  callable function
     *     It will be called for each Movement with these arguments:
     *       itemCallback( $answer_data, $answer_internal_data )
     *     Where the $answer_data is an array with:
     *       id                         string   The Movement identified. Example: 'ASD12312313JHJAJKDJAS'
     *       date                       string   The Movement date - format "Y-m-d\TH:i:s\Z"
     *       amount_gross               string   Example: "25.00"
     *       amount_net                 string   Example: "23.80"
     *       amount_fee                 string   Example: "1.20"
     *       notes                      string   Example: 'BOB TI AMO PRENDI I MIEI SOLDI PER GHANOO PLUS LEENOCS'
     *       notes_xl                   string   Example: 'pippo - pippo@pippi.it - BOB TI AMO PRENDI I MIEI SOLDI PER GHANOO PLUS LEENOCS'
     *     Where the $answer_internal_data is:
     *       paypal_transaction         object   Original PayPal transaction
     *       paypal_transaction_details object   Original PayPal transaction details
     *
     *     Note that if you return FALSE the loop is interrupted, otherwise
     *     you will fire the callback on all remaining movements.
     * @author Valerio Bozzolan
     */
    public static function process($args = [])
    {
        // Declare raw filters.
        $raw_date_start = null; // string|null
        $raw_date_end   = null; // string|null

        // Accept DateTime inputs.
        $date_start     = $args['startDate']     ?? null; // DateTime|null
        $date_end       = $args['endDate']       ?? null; // DateTime|null
        $transaction_id = $args['transactionId'] ?? null; // string|null

        // We do NOT have a Transaction ID.
        // In this case we risk to return a lot of results.
        // Propose decent default dates.

        // As default, start from some days before today.
        if (!$date_start) {
            $date_start = new DateTime();
            $date_start->sub(new DateInterval('P3D'));
            $date_start->setTime(0, 0);
        }

        // As default, add very few days to the start date.
        // This is quite small to do not trigger PayPal result limits.
        if (!$date_end) {
            $date_end = clone $date_start;
            $date_end->add(new DateInterval('P1M'));
            $date_end->setTime(23, 59);
        }

        // PayPal wants this format for dates
        $raw_date_start = $date_start->format('Y-m-d\TH:i:s\Z');
        $raw_date_end   = $date_end  ->format('Y-m-d\TH:i:s\Z');

        if (!isset($args['itemCallback'])) {
            abort(500, "missing itemCallback argument");
        }

        $LOG_PREFIX = sprintf("PayPal processing period %s - %s",
          $raw_date_start,
          $raw_date_end);

        if ($transaction_id) {
            $LOG_PREFIX = sprintf("%s - for transaction ID %s",
              $LOG_PREFIX,
              $transaction_id);
        }

        Log::debug(sprintf("%s - start",
            $LOG_PREFIX));

        // Define PayPAL API request.
        // Some of these may be NULL and that's OK.
        $transactionSearchRequest = new TransactionSearchRequestType();
        $transactionSearchRequest->TransactionID = $transaction_id;
        $transactionSearchRequest->StartDate = $raw_date_start;
        $transactionSearchRequest->EndDate   = $raw_date_end;

        $tranSearchReq = new TransactionSearchReq();
        $tranSearchReq->TransactionSearchRequest = $transactionSearchRequest;

        $paypalService = new PayPalAPIInterfaceServiceService(self::configCredentialsForService());

        $transactionSearchResponse = $paypalService->TransactionSearch($tranSearchReq);
        $errors = $transactionSearchResponse->Errors ?? null;
        if (!$transactionSearchResponse || $transactionSearchResponse->Ack !== 'Success' || isset($errors)) {
            throw new Exception(sprintf("Unexpected response from PayPal. ACK: %s. Errors: %s. Date start: %s. Date end: %s.",
              $transactionSearchResponse->Ack,
              json_encode($errors),
              $raw_date_start,
              $raw_date_end));
        }
        $payment_transactions = $transactionSearchResponse->PaymentTransactions ?? [];
        foreach ($payment_transactions as $trans) {
            $result = self::processSinglePayPalTransactionExpandingDetails($paypalService, $trans, $args['itemCallback']);
            if ($result === false) {
                break;
            }
        }

        Log::debug(sprintf("%s - concluded receiving %d transactions",
          $LOG_PREFIX,
          count($payment_transactions)));
    }

    public static function processSinglePayPalTransactionExpandingDetails($paypalService, $trans, $callback)
    {
        // Some Transactions are "nonsense temporary bank stuff".
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/89
        $nonsense_transaction_types = [
            'Temporary Hold',
            'Authorization',
        ];

        // Some Transactions are "nonsense temporary bank stuff".
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/89
        if (isset($trans->Type) && in_array($trans->Type, $nonsense_transaction_types, true)) {
            return null;
        }

        // Skip nonsense Movements with zero amounts.
        $gross_amount_cents = self::toCents($trans->GrossAmount->value);
        $fee_amount_cents   = self::toCents($trans->FeeAmount->value);
        $net_amount_cents   = self::toCents($trans->NetAmount->value);
        if (!$gross_amount_cents && !$fee_amount_cents && !$net_amount_cents) {
            return null;
        }

        $req = new GetTransactionDetailsRequestType();
        $req->TransactionID = $trans->TransactionID;
        $tranreq = new GetTransactionDetailsReq();
        $tranreq->GetTransactionDetailsRequest = $req;
        $details = $paypalService->GetTransactionDetails($tranreq);
        return self::processSinglePayPalTransactionRaw($trans, $details, $callback);
    }

    public static function processSinglePayPalTransactionRaw($trans, $details, $callback) {

        // TODO: read from configuration.
        $PAYPAL_SELF_EMAIL = 'direttore@linux.it';

        $payment_item_info = $details->PaymentTransactionDetails->PaymentItemInfo;
        $item = $payment_item_info->PaymentItem; // Array of purchased items

        $notes = null;
        if (is_array($item) && $item) {
            $first_item = array_pop($item);
            $notes =
               $payment_item_info->Memo
            ?? $payment_item_info->InvoiceID
            ?? $trans->Type // Fallback for internal transfers with empty Memo. Just to show something.
            ?? null;
        }

        // Sometime PayPal returns every single field with just 'null'. Skip this nonsense data.
//      $gross_amount = $trans->GrossAmount->value ?? $details->PaymentTransactionDetails->PaymentInfo->GrossAmount->value ?? null;
        $gross_amount = $trans->GrossAmount->value ?? throw new Exception("Cannot parse GrossAmount from Transaction {$trans->TransactionID}");
        if(!isset($gross_amount)) {
           return null;
        }

        $date = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $trans->Timestamp ?? $details->Timestamp);

        // Check if this is a payment from a recognizable person.
        $payer = $details->PaymentTransactionDetails->PayerInfo ?? null;
        $payer_email = $payer->Payer ?? null;
        $payer_whatever_name = $payer->PayerName->FirstName ?? $payer->PayerName->LastName ?? $payer->Payer ?? null;
        if (isset($payer_whatever_name) && $payer_email !== $PAYPAL_SELF_EMAIL) {
            $notes_xl = sprintf('%s %s - %s - %s', $payer->PayerName->FirstName, $payer->PayerName->LastName, $payer->Payer, $notes);
        } else {
            $receiver = $details->PaymentTransactionDetails->ReceiverInfo->Receiver ?? null;
            if ($receiver) {
                $notes_xl = sprintf('%s - %s - %s', $receiver, $receiver, $notes);
            } else {
                $notes_xl = $notes;
            }
        }

        // The Fee in this field is usually negative.
        // Note that the Fee in the details, instead, is always absolute.
        // Since 2024-07-28 we adopted the negative value (instead of absolute). This is a desired breaking change.
        $fee = $trans->FeeAmount->value ?? throw new Exception("Cannot parse FeeAmount from Transaction {$trans->TransactionID}");

        $answer_data = [
            'id'           => $trans->TransactionID,
            'date'         => $date->format('Y-m-d\TH:i:s\Z'),
            'amount_gross' => $trans->GrossAmount->value,
            'amount_net'   => $trans->NetAmount  ->value,
            'amount_fee'   => $trans->FeeAmount  ->value,
            'notes'        => $notes,
            'notes_xl'     => $notes_xl,
        ];
        $answer_internal_data = [
            'paypal_transaction'         => $trans,
            'paypal_transaction_details' => $details,
        ];
        $result = call_user_func($callback, $answer_data, $answer_internal_data);
        if ($result === false) {
            return false;
        }
    }


    /**
     * Get an array of credentials for PayPalAPIInterfaceServiceService
     *
     * @return array
     */
    private static function configCredentialsForService()
    {
        // no credentials no party
        $config = self::validatedConfig();

        // initialize config from given credentials
        $paypal_config = [
               'mode'            => 'live',
               'acct1.UserName'  => $config['username'],
               'acct1.Password'  => $config['password'],
               'acct1.Signature' => $config['signature'],
        ];

        return $paypal_config;
    }

    /**
     * Check if you have a valid configuration
     *
     * Do not care about whatever issue about it
     *
     * @return boolean
     */
    public static function hasValidConfig()
    {
        $ok = false;

        try {

            // this can throw an exception
            self::validatedConfig();

            // yuppie!
            $ok = true;
        } catch (\Exception $e) {

            // oh now :(
        }

        return $ok;
    }

    /**
     * Get the whole validated PayPal service configuration
     *
     * Note that this throws exceptions if something is wrong.
     *
     * @return array
     */
    public static function validatedConfig()
    {

        // no PayPal info no party
        $info = config('services.paypal');
        if (!$info) {
            throw new \Exception("missing PayPal configuration - check your ENV file");
        }

        // mandatory fields with the related ENV file (check config/services.php)
        $mandatory_fields_and_env = [

            // useful to login in PayPal APIs
            'username'  => 'PAYPAL_USERNAME',
            'password'  => 'PAYPAL_PASSWORD',
            'signature' => 'PAYPAL_SIGNATURE',

            // useful to keep a relation to the Bank ID in the database
            'bankid'    => 'PAYPAL_BANKID',
        ];

        // check each mandatory field
        foreach ($mandatory_fields_and_env as $field => $env) {

            // no field no party
            if (!isset($info[ $field ])) {

                // be very verbose because I can be totally drunk when configuring this stuff
                throw new \Exception(sprintf(
                    "missing PayPal configuration field '%s'. Check %s in your .env file and read the fantastic manual",
                    $field,
                    $env
                ));
            }
        }

        return $info;
    }

    private static function toCents(string $v) {
        $v = str_replace('.', '', $v);
        return (int) $v;
    }
}
