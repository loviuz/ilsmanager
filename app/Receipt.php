<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use PDF;

/**
 * A Receipt is a proof demonstrating that somebody executed
 * a money transfer (Movement) to your organization.
 * For example a single Movement consisting in 100 EUR
 * can generate a single Receipt with the following subject:
 * "Saldo quota 2024, 2025, 2026, 2027".
 * A Receipt is always related to a PDF file.
 */
class Receipt extends Model
{
    public function movement()
    {
        return $this->belongsTo('App\Movement');
    }

    public function getFullNumberAttribute()
    {
        list($year, $month, $day) = explode('-', $this->date);
        return sprintf('%s/%s', $this->number, $year);
    }

    public function getLinkAttribute()
    {
        return route('receipt.show', $this->id);
    }

    public function getPathAttribute()
    {
        $filename = sprintf('%s.pdf', str_replace('/', '_', $this->full_number));
        return storage_path('receipts/' . $filename);
    }

    public function generate()
    {
        $pdf = PDF::loadView('receipt.pdf', ['object' => $this]);
        $pdf->save($this->path);
    }
}
