<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    use ILSModel;

    /**
     * Get the generic name of this entity, singular.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public function getEntityNameSingular()
    {
        return "Sponsor";
    }

    /**
     * Get the object short name.
     * This may be shown in a breadcrumb.
     * @return string
     */
    public function getObjectShortName()
    {
        return $this->name;
    }

    public function attachFile($file)
    {
        $path = self::logoPath();
        $file->move($path, $this->id);
    }

    /**
     * Check whenever this sponsor has an uploaded logo.
     * @return bool
     */
    public function hasLogo() {
        return file_exists(self::logoPath($this->id));
    }

    /**
     * Get the path to the logo of this Sponsor.
     * Specity a NULL $id to just get the directory.
     * @param $id int|null
     * @return string
     */
    public static function logoPath($id = null) {
        if ($id === null) {
            $id = '';
        }

        // Do not allow in any way to get out the base directory.
        $id = (string) $id;
        $id = basename($id);
        return storage_path('sponsors/' . $id);
    }

    public static function getTotalExpiring(){
        return \App\Sponsor::where('expiration', '<', date('Y-m-d', strtotime('+4 months')))->where('expiration', '>=', date('Y-m-d'))->count();
    }

    public static function getTotalExpired(){
        return \App\Sponsor::where('expiration', '<', date('Y-m-d'))->count();
    }
}
