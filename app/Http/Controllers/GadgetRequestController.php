<?php

namespace App\Http\Controllers;

use App\GadgetRequest;
use App\GadgetRequestStatus;
use App\GadgetType;
use Auth;
use Illuminate\Http\Request;

class GadgetRequestController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => GadgetRequest::class,
            'view_folder' => 'gadget-request',
        ]);

        // No feature, no party.
        if( !GadgetRequest::is_feature_enabled()) {
            abort(404);
        }
    }

    /**
     * @return string
     */
    protected function getIndexPageTitle() {
        return "Richieste Spedizione Gadget";
    }

    /**
     * Create a new, un-persisted, object.
     */
    protected function createObject(Request $request)
    {
        $object = new GadgetRequest;
        if (Auth::check()) {
            $auth_user = Auth::user();
            $object->user_id = $auth_user->id;
            $object->name = $auth_user->name;
            $object->surname = $auth_user->surname;
            $object->street = $auth_user->shipping_street ?? $auth_user->address_street;
            $object->postal_code = $auth_user->shipping_zip ?? $auth_user->address_zip;
            $object->province = $auth_user->shipping_prov ?? $auth_user->address_prov;
            $object->city = $auth_user->shipping_place ?? $auth_user->address_place;
        }
        return $object;
    }

    /**
     * Create a Query object, with default order.
     */
    protected function queryList(Request $request)
    {
        $query = parent::queryList($request)
            ->with('status')
            ->with('user');

        $args = $this->getExtraFilterArguments($request);
        if ($args['selected_status_ids']) {
            $query->whereIn('status_id', $args['selected_status_ids']);
        }

        // Show my stuff.
        if ($args['my_requests'] === '1' && Auth::check()) {
            $query->where('user_id', Auth::user()->id);
        }

        return $query;
    }

    public function getIndexArguments(Request $request)
    {
        $args = array_merge(
            parent::getIndexArguments($request),
            $this->getExtraFilterArguments($request)
        );

        // Show a multi-tracking URL, if possible.
        // This is shown to everybody, if you can see all details.
        $args['multi_tracking_url'] = null;
        if (Auth::check()) {
            $tracking_codes = [];
            foreach ($args['objects'] as $object) {
                if (Auth::check() && $object->tracking_code && Auth::user()->can('view', $object)) {
                    $tracking_codes[] = $object->tracking_code;
                }
            }
            if ($tracking_codes) {
                $args['multi_tracking_url'] =
                    GadgetRequest::postal_multi_tracker_URL($tracking_codes);
            }
        }

        return $args;
    }

    /**
     * Get filters in use by the page and shown in the index view.
     */
    private function getExtraFilterArguments(Request $request)
    {
        $args['all_statuses'] = GadgetRequestStatus::all();
        $args['gadgetTypes'] = GadgetType::where('is_active', true)->get();

        $selected_status_uids = $request->input('status');

        // Show a default filter by status.
        if (!is_array($selected_status_uids)) {
            // TODO: Add an option "Is default filter" in table "gadget_request_statuses".
            //       So we don't hardcode database stuff here.
            //       Nobody probably will ever do this. That's OK. -2024-11-12 -bozz
            $selected_status_uids = GadgetRequestStatus::getActiveStatusUIDs();
        }

        $args['selected_status_ids'] = [];
        foreach ($selected_status_uids as $selected_status_uid) {
            $args['selected_status_ids'][] = GadgetRequestStatus::findByUID($selected_status_uid)->id;
        }

        // Whenever we should only show my stuff.
        $args['my_requests'] = $request->input('my_requests', $this->getDefaultFilterMyRequests());

        return $args;
    }

    public function thankYou()
    {
        return view($this->view_folder . '.thank_you');
    }

    public function create(Request $request)
    {
        // This is the creation form for anonymous people.
        return view($this->view_folder . '.create', $this->getIndexArguments($request));
    }

    public function store(Request $request)
    {
        $this->authorize('create', $this->classname);
        $validations = $this->defaultValidations(null);
        $request->validate($validations);

        $object = new $this->classname;
        $this->requestToObject($request, $object);
        $object->save();
        $this->afterSaving($request, $object);

        if (Auth::check()) {
            return redirect()->route('gadget-request.index')
                ->with('success', 'Richiesta di gadget creata con successo.');
        }

        return redirect()->route('gadget-request.thank-you')
            ->with('success', 'Richiesta di gadget creata con successo.');
    }

    public function getEditArguments($id)
    {
        // Imita parent
        $args = parent::getEditArguments($id);

        // Recupera i tipi di gadget attivi
        $args['gadgetTypes'] = GadgetType::where('is_active', true)->get();

        // Passa la richiesta e i tipi di gadget alla vista
        return $args;
    }

    // Controller method to handle fetching the request by token
    public function showByToken($token)
    {
        $gadgetRequest = GadgetRequest::where('token', $token)->firstOrFail();

        return view($this->view_folder . '.show', compact('gadgetRequest'));
    }

    protected function defaultValidations($object)
    {
        return [
            'email' => 'nullable|email',
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'postal_code' => 'required|string|max:10',
            'city' => 'required|string|max:255',
            'province' => 'required|string|max:255',
            'street' => 'required|string|max:255',
            'distribution_location' => 'nullable|string',
            'subscribe_newsletter' => 'boolean',
            'gadget_type' => 'required|array',
            'gadget_type.*' => 'integer|exists:gadget_types,id', // Validazione di ogni elemento dell'array
            'status_id' => 'nullable|string|in:' . implode(',', GadgetRequestStatus::pluck('id')->toArray()),
        ];
    }

    protected function requestToObject($request, $object): void
    {
        $object->name = $request->input('name');
        $object->surname = $request->input('surname');
        $object->street = $request->input('street');
        $object->postal_code = $request->input('postal_code');
        $object->city = $request->input('city');
        $object->province = $request->input('province');
        $object->distribution_location = $request->input('distribution_location');
        $object->subscribe_newsletter = $request->input('subscribe_newsletter');
        if (!isset($object->id)) {
            // Creation

            if (Auth::check()) {
                // Registered user.
                // Create as valid as default.
                $object->user_id = Auth::id();
                $object->email = null;
                $object->status_id = GadgetRequestStatus::findByUID('valid')->id;
            } else {
                // Unregistered user.
                // Create as pending as default.
                $object->email = $request->input('email');
                $object->status_id = GadgetRequestStatus::findByUID('pending')->id;
            }
        } else {
            // Update

            // Only allow unregistered users to set/update the email.
            if ($object->user_id) {
                $object->email = null;
            } else {
                $object->email = $request->input('email');
            }

            // Allow shipper and admins to review.
            if (Auth::user()->can('review', $object)) {
                $object->status_id     = $request->input('status_id',     $object->status_id);
                $object->tracking_code = $request->input('tracking_code', $object->tracking_code);
            }
        }
    }

    protected function afterSaving($request, $object)
    {
        $gadgetTypeIds = $request->input('gadget_type', []);
        $object->gadgetTypes()->sync($gadgetTypeIds);
    }

    protected function defaultSortingColumn()
    {
        return 'id';
    }

    /**
     * Get the default filter for the "my requests" filter.
     * @return string
     */
    private function getDefaultFilterMyRequests()
    {
        // Reviewers and admins may want to see everything as default.
        if (Auth::check() && Auth::user()->can('review', new GadgetRequest)) {
            return '0';
        }

        // Normal users may want to only see their stuff.
        return '1';
    }

}

