<?php

namespace App\Http\Controllers;

use App\Movement;
use App\Bank;
use App\Fee;

use Illuminate\Http\Request;

class BankController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => Bank::class,
            'view_folder' => 'bank'
        ]);
    }

    /**
     * Get the generic page title of this controller.
     * This may be shown in the breadcrumb.
     * @return string
     */
    protected function getIndexPageTitle() {
        return "Banche";
    }

    protected function defaultValidations($object)
    {
        return [
            'name' => 'required|max:255',
            'identifier' => 'required|max:255',
        ];
    }

    protected function requestToObject($request, $object): void
    {
        $object->name = $request->input('name');
        $object->type = $request->input('type');
        $object->identifier = $request->input('identifier');
    }

}
