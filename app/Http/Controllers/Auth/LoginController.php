<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers  {
        login as realLogin;
    }

    /**
     * Where to redirect users after login.
     */
    protected string $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username(): string
    {
        return 'username';
    }

    public function login(Request $request): RedirectResponse
    {
        $username = $request->input('username');

        $user = User::where('username', $username)->first();
        if ($user && $user->canLogin()) {
            return $this->realLogin($request);
        } else {
            return redirect()->route('login')
                ->withErrors(['username' => 'Credenziali errate.']);
        }
    }
}
