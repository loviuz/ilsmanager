<?php

namespace App\Http\Controllers;

class SponsorController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Sponsor',
            'view_folder' => 'sponsor'
        ]);
    }

    protected function requestToObject($request, $object): void
    {
        $fields = ['name', 'website', 'email', 'expiration', 'notes'];
        $object = $this->fitObject($object, $fields, $request);

        // The schema does not allow NULL notes.
        if (is_null($object->notes)) {
            $object->notes = '';
        }
    }

    protected function defaultValidations($object)
    {
        $ret = [
            'name' => 'required|max:255',
            'website' => 'required|max:255',
            'email' => 'required|email|max:255',
            'expiration' => 'required|max:255',
        ];

        return $ret;
    }

    protected function afterSaving($request, $object)
    {
        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $object->attachFile($logo);
        }
    }

    public function logo($id)
    {
        $path = \App\Sponsor::logoPath($id);
        if (file_exists($path)) {
            return response()->download($path);
        }
        else {
            return '';
        }
    }

    protected function defaultSortingColumn()
    {
        return 'expiration';
    }

}
