<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

use Auth;
use Hash;
use Illuminate\View\View;
use Log;

use App\Bank;
use App\Section;
use App\User;
use App\UserType;
use App\Config;

class UserController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => User::class,
            'view_folder' => 'user',
        ]);
    }

    /**
     * Get the generic page title of this controller.
     * This may be shown in the breadcrumb.
     * @return string
     */
    protected function getIndexPageTitle()
    {
        return "Utenze";
    }

    /**
     * Get the page default elements limit.
     * So, if the user has not specified anything, this is the default limit.
     * @return int
     */
    protected function getPageElementsDefaultLimit() {
        return 1000;
    }

    public function index(Request $request): View
    {
        return parent::index($request)
            ->with($this->getExtraArguments($request));
    }

    protected function getEditArguments($id)
    {
        $args = parent::getEditArguments($id);
        $user = $args['object'];

        // Whenever the current user can change the Section.
        // Normal users usually can always change their section...
        // but referents should not, for data protection reasons.
        $args['is_section_editable'] = $this->isSectionEditable();

        $paypal_bank = Bank::where('type', 'paypal')->first();

        // Generate fee URL.
        $args['paypal_fee_url'] = null;
        if ($paypal_bank) {
            $args['paypal_fee_url'] = $paypal_bank->getPayPalFeeURL([
                'user' => $user,
            ]) ;
        }

        // Generate donation URL.
        $args['paypal_donate_url'] = null;
        if ($paypal_bank) {
            $args['paypal_donate_url'] = $paypal_bank->getPayPalDonateURL([
                'user' => $user,
            ]) ;
        }

        return $args;
	  }

    protected function getExtraArguments( ?Request $request = null )
    {
        $sectionFilter = null;
        $residenceProvFilter = null;
        $statusFilter = null;
        $typeFilter = null;
        if ($request) {
            $sectionFilter = $request->get('section');
            $residenceProvFilter = $request->get('residence_prov');
            $statusFilter = $request->get('status');
            $typeFilter = $request->get('type');
        }

        // Have a good default. But allow to skip this default.
        if ($statusFilter === null) {
            $statusFilter = 'active';
        } elseif ($statusFilter === '-') {
            $statusFilter = null;
        }

        $activeUsersCount = User::query()
            ->active()
            ->count();

        $pendingUsersCount = User::query()
            ->pending()
            ->count();

        $activeAssociationUsersCount = User::query()
            ->where('type', 'association')
            ->active()
            ->count();

        // TODO: maybe create a dedicated user status that indicates.
        $pendingUsersThatCanLoginCount =
            User::query()
              ->pendingWithPaidFee()
              ->count();

        $allUsersThatCanLoginCount =
            $pendingUsersThatCanLoginCount +
            $activeUsersCount;

        return [
            'sectionFilter' => $sectionFilter,
            'residenceProvFilter' => $residenceProvFilter,
            'statusFilter' => $statusFilter,
            'typeFilter' => $typeFilter,

            'allUsersThatCanLoginCount' => $allUsersThatCanLoginCount,
            'activeUsersCount' => $activeUsersCount,
            'pendingUsersCount' => $pendingUsersCount,
            'activeAssociationUsersCount' => $activeAssociationUsersCount,
        ];
    }

    protected function query(Request $request)
    {
				$query = parent::query($request);

        $query->with('section');

        $extra_args = $this->getExtraArguments($request);
        $sectionFilter = $extra_args['sectionFilter'];
        $residenceProvFilter = $extra_args['residenceProvFilter'];
        $statusFilter = $extra_args['statusFilter'];
        $typeFilter = $extra_args['typeFilter'];

        // Default order. TODO: allow to customize.
        $query->orderBy('surname');

        // Allow to filter by a section, but also to get "no sections".
        if (!empty($sectionFilter)) {
            if ($sectionFilter === '-none-') {
                // TODO: set nullable
                // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/119
                $query->where('section_id', 0);
            } else {
                $query->whereHas('section', function ($query) use ($sectionFilter) {
                    $query->where('id', $sectionFilter);
                });
            }
        }

        if (!empty($residenceProvFilter)) {
            $query->where('address_prov', $residenceProvFilter);
        }

        // Filter by user status. Example: 'active'.
        if (!empty($statusFilter)) {
            $query->where('status', $statusFilter);
        }

        // Filter by user type. Example: 'association'.
        if ($typeFilter) {
            // Extra validation, just because we can.
            UserType::find_by_identifier_or_fail($typeFilter);
            // Apply filter.
            $query->where('type', $typeFilter);
        }

        return $query;
    }

    protected function requestToObject($request, $object): void
    {
        $fields = ['name', 'surname', 'email', 'phone', 'website', 'taxcode', 'birth_place', 'birth_prov', 'birth_date', 'address_street', 'address_place', 'address_zip', 'address_prov', 'shipping_name', 'shipping_street', 'shipping_place', 'shipping_zip', 'shipping_prov', 'size'];
        $object = $this->fitObject($object, $fields, $request);

        $object->volunteer = $request->has('volunteer');

        if ($this->isSectionEditable()) {
            // TODO: set null instead of zero
            // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/143
            $object->section_id = $request->input('section_id', null);
        }

        if ($request->user()->hasRole('admin')) {
            $fields = ['username', 'type', 'status', 'notes', 'request_at', 'approved_at', 'expelled_at'];
            $object = $this->fitObject($object, $fields, $request);

            $roles = $request->input('roles', []);
            $object->roles()->sync($roles);
        }

        if (empty($object->notes)) {
            $object->notes = '';
        }

        if (Config::getConfig('custom_email_aliases') == '1') {
            $email_type = $request->input('email_behaviour', null);
            if (!is_null($email_type)) {
                if ($email_type == 'alias') {
                    $object->setConfig('email_behaviour', 'alias');
                }
                else {
                    $object->setConfig('email_behaviour', 'inbox');
                    $new_email_password = $request->input('email_password', null);
                    if (!empty($new_email_password)) {
                        Log::debug('Cambiata password email utente ' . $object->username);
                        $object->setConfig('email_password', mailPasswordHash($new_email_password));
                    }
                }
            }
        }

        $password = $request->input('password');
        if (!empty($password)) {
            $object->password = Hash::make($password);
        }
        else {
            if ($object->exists == false) {
                $object->password = Hash::make(Str::random(10));
            }
        }
    }

    protected function defaultValidations($object)
    {
        $ret = [
            'name' => ['max:255'],
            'surname' => ['max:255'],
            'email' => 'required|email|max:255',
            'address_street' => 'required|max:255',
            'address_place' => 'required|max:255',
            'address_zip' => 'required|max:255',
            'address_prov' => 'required|max:2',
        ];

        if ($object && $object->type != 'guest') {
            $ret['name'][] = 'required';
            $ret['surname'][] = 'required';
        }

        if (Auth::user()->hasRole('admin')) {
            if (is_null($object)) {
                $ret['username'] = 'required|unique:users|max:255';
            }
            else {
                $ret['username'] = [
                    'required',
                    Rule::unique('users')->ignore($object->id),
                ];
            }

            $ret = array_merge($ret, [
                'type' => 'required|in:regular,association,guest',
                'taxcode' => 'required|max:255',
                'birth_place' => 'required|max:255',
                'birth_prov' => 'required|max:2',
                'birth_date' => 'required|max:255',
            ]);
        }

        return $ret;
    }

    protected function defaultSortingColumn()
    {
        return 'surname';
    }

    public function approve(Request $request)
    {
        $this->checkAuth();
        $approved = $request->input('approved', []);
        User::where('status', 'pending')->whereIn('id', $approved)->update(['status' => 'active', 'approved_at' => date('Y-m-d')]);
        return redirect()->route('user.index');
    }

    public function bypass(Request $request, $id)
    {
        if ($request->user()->hasRole('admin')) {
            Auth::logout();
            Auth::loginUsingId($id);
        }

        return redirect('/');
    }

    /**
     * Check if the section can be changed or not from the viewer.
     * @TODO: migrate this to a Policy
     * @return bool
     */
    private function isSectionEditable()
    {
        $user = Auth::user();

        // Always allow to set a section, if it was not already selected.
        // Always allow admins.
        if (!$user->section || $user->hasRole('admin')) {
            return true;
        }

        // Don't allow section referents to change their personal section whenever they want.
        // This is needed since referents has extra visibility scope on their section,
        // so, they should change section only after extra care by admins.
        return !$user->hasRole('referent');
    }

    /**
     * Return the user actually logged
     */
    public function profile(Request $request)
    {
        return response()->json($request->user());
    }

}
