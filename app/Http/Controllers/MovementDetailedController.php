<?php
/**
 * ILS Manager
 * Copyright (C) 2021-2024 Italian Linux Society, contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\AccountRow;
use App\Bank;
use App\Fee;
use App\Movement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use stdClass;

class MovementDetailedController extends EditController
{
    protected $classname = Movement::class;
    protected $view_folder = 'movement-detailed';

    public function exportAllCsv(Request $request)
    {
        // Require administrators.
        $this->checkAuth();

        // Create Movement DTOs.
        // This contain all the information ready for export.
        $args = $this->getIndexArguments($request);
        $movements_dtos = $args['movements_dto'];

        // Create a temporary file.
        $tmp_file_path = tempnam(sys_get_temp_dir(), 'tempmovementdetailedcsv_');
        $tmp_file_pointer = fopen($tmp_file_path, 'w');

        // Generate CSV file structure with header.
        fputcsv($tmp_file_pointer, $this->createHeaderExportData());
        foreach ($movements_dtos as $movement_dto) {
            fputcsv($tmp_file_pointer, $this->entryDtoToExportData($movement_dto));
        }
        fclose($tmp_file_pointer);

        // Download the CSV file and nuke the temporary file.
        return Response::download($tmp_file_path, 'movements-export-all.csv', [
            'ResponseContentType' => 'text/csv',
        ])
            ->deleteFileAfterSend(true);
    }

    /**
     * Get the page elements limit.
     *
     * @return int
     */
    protected function getPageElementsDefaultLimit()
    {
        return 1000;
    }
    
    protected function getIndexRequestArgs(Request $request): array {
        $args = parent::getIndexRequestArgs($request);

        $args['bank_id'] = $request->input('bank_id');
        
        $args['selected_account_ids'] = $request->input('account', []);

        // The default value of the "with_start_of_year" checkbox is true
        // on the default view, but we must preserve it's false value when
        // the user has submitted something but leaving this checkbox unchecked.
        $is_default_view = count($request->all()) < 2;
        $args['with_start_of_year'] = $request->input('with_start_of_year', $is_default_view ? true : false);
        
        // Pick the current year as default one.
        // Allow to unset.
        $args['year'] = $request->input('year');
        if ($args['year'] === '-') {
            $args['year'] = null;
        } else {
            $args['year'] = (int) $args['year'];
            if ($args['year'] <= 0) {
                $args['year'] = date('Y');
            }
        }

        return $args;
    }
    
    protected function query(Request $request)
    {
        $movements_query = parent::query($request);
        
        $request_args = $this->getIndexRequestArgs($request);

        $bank_id = $request_args['bank_id'];
        if ($bank_id) {
            $movements_query->where('bank_id', $bank_id);
        }

        // Allow to exclude the balance at the start of this year, or not.
        if ($request_args['with_start_of_year']) {
            $movements_query->whereNull('start_of_year');
        }

        // Also include AccountRow(s).
        // The with() is quite efficient and avoids N+1 performance issues.
        $movements_query->with('account_rows', function ($query) use ($request_args) {

            // Filter AccountRow(s) by Account.
            if ($request_args['selected_account_ids']) {
                $query->whereIn('account_id', $request_args['selected_account_ids']);
            }
        });

        // As default, filter by current year.
        // Allow to filter by all years, setting '-'.
        $year = $request_args['year'];
        if ($year) {
            $movements_query->whereYear('date', $year);
        }

        return $movements_query;
    }

    protected function getIndexArguments(Request $request)
    {
        $args = parent::getIndexArguments($request);

        $banks = Bank::all();

        $query = $args['query'];
        $movements = $args['objects'];

        // Batch-Download all Fees, related to the available AccountRows.
        // This block avoids "N+1" performance issues.
        $fee_by_account_row = (function () use ($movements) {
            $account_row_ids = [];
            foreach ($movements as $movement) {
                foreach ($movement->account_rows as $i => $account_row) {
                    $account_row_ids[] = $account_row->id;
                }
            }

            $fees = [];
            if ($account_row_ids) {
                $fees = Fee::whereIn('account_row_id', $account_row_ids)
                    ->get();
            }

            // Return Fees indexed by AccountRow.
            $indexed_fees = [];
            foreach ($fees as $fee) {
                $indexed_fees[$fee->account_row_id] = $fee;
            }

            return $indexed_fees;
        })();

        $account_rows_amount_sum = '0.00';
        $movements_amount_sum = '0.00';

        // Create some Data Transfer Objects to simplify the construction of with Movement and (eventually) its AccountRow.
        $movements_dto = [];
        $is_first_ar = true;
        foreach ($movements as $movement) {
            $is_first_ar = true;
            $movements_amount_sum = bcadd($movements_amount_sum, $movement->amount, 2);
            foreach ($movement->account_rows as $account_row) {
                $movements_dto[] = [
                    'movement' => $movement,
                    'account_row' => $account_row,
                    'account_row_first' => $is_first_ar,
                    'fee' => $account_row ? $fee_by_account_row[$account_row->id] ?? null : null,
                ];
                $is_first_ar = false;
                $account_rows_amount_sum = bcadd($account_rows_amount_sum, $account_row->amount, 2);
            }
        }

        $args = array_replace($args, [
            'movements_dto' => $movements_dto,
            'movements' => $movements,
            'banks' => $banks,
            'movements_amount_sum' => $movements_amount_sum,
            'account_rows_amount_sum' => $account_rows_amount_sum,
        ]);

        return $args;
    }

    protected function defaultSortingColumn()
    {
        return 'date';
    }

    protected function defaultValidations($object)
    {
        // TODO: Implement defaultValidations() method.
    }

    protected function requestToObject($request, $object): void
    {
        // TODO: Implement requestToObject() method.
    }

    private function entryDtoToExportData($dto_line): array
    {
        $data = [];
        $data[] = $dto_line['movement']->id;
        $data[] = $dto_line['movement']->bank->id;
        $data[] = $dto_line['movement']->bank->name;
        $data[] = $dto_line['movement']->date;
        $data[] = $dto_line['movement']->identifier;
        $data[] = $dto_line['movement']->amount;
        $data[] = $dto_line['movement']->amount_original;
        $data[] = $dto_line['movement']->reviewed;
        $data[] = $dto_line['movement']->notes;
        $data[] = $dto_line['account_row']->id;
        $data[] = $dto_line['account_row']->account->id ?? null;
        $data[] = $dto_line['account_row']->account->name ?? null;
        $data[] = $dto_line['account_row']->account->printableName ?? null;
        $data[] = $dto_line['account_row']->amount;
        $data[] = $dto_line['account_row']->notes;

        return $data;
    }

    private function createHeaderExportData(): array
    {
        $movement = new stdClass;
        $movement->bank = new stdClass;
        $account_row = new stdClass;
        $account_row->account = new stdClass;
        $movement->bank->id = 'Movement.Bank.ID';
        $movement->bank->name = 'Movement.Bank.Name';
        $movement->id = 'Movement.ID';
        $movement->date = 'Movement.Date';
        $movement->notes = 'Movement.Notes';
        $movement->identifier = 'Movement.Identifier';
        $movement->amount = 'Movement.Amount';
        $movement->amount_original = 'Movement.AmountOriginal';
        $movement->reviewed = 'Movement.Reviewed';
        $account_row->id = 'AccountRow.ID';
        $account_row->account->id = 'AccountRow.Account.ID';
        $account_row->account->name = 'AccountRow.Account.Name';
        $account_row->account->printableName = 'AccountRow.Account.CompleteName';
        $account_row->amount = 'AccountRow.Amount';
        $account_row->notes = 'AccountRow.Notes';

        return $this->entryDtoToExportData([
            'movement' => $movement,
            'account_row' => $account_row,
        ]);
    }
}
