<?php

namespace App\Http\Controllers;

use App\Refund;
use App\RefundStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RefundController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => Refund::class,
            'view_folder' => 'refund',
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'amount' => 'required|numeric',
            'title' => 'nullable|string',
            'notes' => 'required',
            'refunded' => 'numeric',
            'report_url' => 'nullable|url',
        ];
    }

    public function downloadReceipt(Request $request, string $id, string $receipt)
    {
        $refund = $this->getObject($id);

        // Be extra-sure to be authorized to see this.
        $this->authorize('view', $refund);

        // Do not allow to specify any directory.
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/123
        $receipt_safe = basename($receipt);

        // Generate the complete path to this receipt and cause download.
        $receipt_safe_path = $refund->attachments_receipts_path . '/' . $receipt_safe;

        return response()->download($receipt_safe_path);
    }

    public function downloadQuote(Request $request, string $id, string $quote)
    {
        $refund = $this->getObject($id);

        // Be extra-sure to be authorized to see this.
        $this->authorize('view', $refund);

        // Do not allow to specify any directory.
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/123
        $quote_safe = basename($quote);

        // Generate the complete path to this quote and cause download.
        $quote_safe_path = $refund->attachments_quotes_path . '/' . $quote_safe;

        return response()->download($quote_safe_path);
    }

    /**
     * Get the generic page title of this controller.
     * This may be shown in the breadcrumb.
     *
     * @return string
     */
    protected function getIndexPageTitle()
    {
        return 'Rimborsi Spese';
    }

    /**
     * Check if the current user is allowed to review a generic refund.
     * Just a shortcut for the RefundPolicy.
     */
    protected function userCanReview(?Refund $refund = null): bool
    {
        // In the list we don't really have an object, but we need that,
        // to trigger the RefundPolicy#review().
        if ( !$refund) {
            $redund = new Refund;
        }
        return Auth::user()->can('review', $refund);
    }

    protected function getIndexRequestArgs(Request $request): array
    {
        $args = parent::getIndexRequestArgs($request);

        // Allow to select a custom year.
        $args['year'] = $request->input('year');

        // Set some default statuses, but only if this is the home.
        $is_home = count($request->all()) < 2;
        $args['selected_refund_status_uids'] = $request->input('status', []);
        if (! is_array($args['selected_refund_status_uids']) && $is_home) {
            $args['selected_refund_status_uids'] = [];
            if (Auth::user()->can('review', new Refund)) {
                // Default filters for reviewers :)
                $args['selected_refund_status_uids'][] = 'waiting';   // Check incoming stuff to be approved.
                $args['selected_refund_status_uids'][] = 'approved';  // Check approved stuff to be paid.
                $args['selected_refund_status_uids'][] = 'paid';      // Check paid stuff to be also verified.
            } else {
                // Default filters for normal users :)
                $args['selected_refund_status_uids'][] = 'waiting';   // See my pending stuff.
                $args['selected_refund_status_uids'][] = 'approved';  // See my approved stuff.
                $args['selected_refund_status_uids'][] = 'blocked';   // See my blocked stuff so I can change things to make the board happier.
            }
        }

        $args['selected_statuses'] = [];
        foreach ($args['selected_refund_status_uids'] as $selected_refund_status_uid) {
            $args['selected_statuses'][] = \App\RefundStatus::findByUID($selected_refund_status_uid);
        }

        return $args;
    }

    protected function getIndexArguments(Request $request)
    {
        $args = parent::getIndexArguments($request);

        $args['all_statuses'] = \App\RefundStatus::all();

        $args['report_options'] = $this->getReportURLOptions();

        // All selectable users.
        $args['users'] = [];
        if ($this->userCanReview()) {
            $args['users'] = \App\User::has('refunds')->orderBy('username', 'asc')->get();
        }

        // All selectable sections.
        $args['sections'] = \App\Section::orderBy('city', 'asc')->get();

        // Don't allow to select nonsense filter years.
        $min_date = \App\Refund::min('date');
        $max_date = \App\Refund::max('date');
        $args['min_year'] = date('Y', strtotime($min_date));
        $args['max_year'] = date('Y', strtotime($max_date));

        // Calculate footer.
        $args['total'] = 0;
        $args['total_refunded'] = 0;
        foreach ($args['objects'] as $refund) {
            $args['total'] += $refund->amount;
            $args['total_refunded'] += ($refund->amount - ($refund->refundStatus->completed ? $refund->amount : 0));
        }

        return $args;
    }

    /**
     * Create a Query object, with default order.
     */
    protected function queryList(Request $request)
    {
        $query = parent::queryList($request);

        $args = $this->getIndexRequestArgs($request);

        // Filter by year.
        if ($args['year']) {
            $query->whereYear('date', $args['year']);
        }

        // Filter by Report availability.
        if ($request->input('report') === 'yes') {
            $query->whereNotNull('report_url');
        } elseif ($request->input('report') === 'no') {
            $query->whereNull('report_url');
        }

        // Filter by statues.
        if ($args['selected_statuses']) {
            $selected_status_ids = [];
            foreach ($args['selected_statuses'] as $status) {
                $selected_status_ids[$status->id] = $status->id;
            }
            $query->whereIn('refunded', $selected_status_ids);
        }

        // Filter by Section ID.
        if ($request->input('section_id')) {
            $query->where('section_id', $request->input('section_id'));
        }

        return $query;
    }

    protected function requestToObject($request, $object): void
    {
        $object->title = $request->input('title');
        $object->date = $request->input('date');
        $object->section_id = $request->input('section_id');
        $object->notes = $request->input('notes');
        $object->report_url = $request->input('report_url');

        if ($this->userCanReview($object)) {
            // The Status may not be expressed in creation mode.
            if ($request->has('refunded')) {
                $object->refunded = (int) $request->input('refunded');
            }

            $object->user_id = $request->input('user_id', 0);
            $object->admin_notes = $request->input('admin_notes');
        } elseif ($request->user()->can('update', $object)) {
            $object->user_id = $object->user_id ?: $request->input('user_id', $request->user()->id);
            $object->admin_notes = $object->admin_notes ?: '';
        } else {
            $object->user_id = $request->user()->id;
            $object->admin_notes = $object->admin_notes ?: '';
        }

        $status = null;
        if ($object->refunded) {
            $status = RefundStatus::findByID($object->refunded);
        }

        // Do not allow normal users to change an approved Amount.
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/138
        if ($request->has('amount')) {
            if (! $status || $status->isEditableByUser($request->user())) {
                $object->amount = $request->input('amount');
            }
        }
        
        // TODO: allow NULL instead of nonsense zero...
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/commit/ae798dcc884bac379b4690cebe3b8b53cc3ec676
        if($object->section_id === null) {
            $object->section_id = 0;
        }
    }

    protected function defaultSortingColumn()
    {
        return 'date';
    }

    protected function afterSaving($request, $object)
    {
        if ($request->hasFile('file_receipt')) {
            $files = $request->file('file_receipt');
            foreach ($files as $f) {
                $object->attachReceipt($f);
            }
        }

        if ($request->hasFile('file_quote')) {
            $files = $request->file('file_quote');
            foreach ($files as $f) {
                $object->attachQuote($f);
            }
        }
    }

    /**
     * Get all possible options for the "With report" filter.
     *
     * @return array
     */
    private function getReportURLOptions()
    {
        return [
            '-' => 'Tutti',
            'yes' => 'Report presente',
            'no' => 'Report ancora da inserire',
        ];
    }
}
