<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $fillable = [
        'user_id',
        'command_name',
        'send_date',
        'note',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope a query to only include a specific command name.
     */
    public function scopeCommandName(Builder $query, string $command_name): void
    {
        $query->where('command_name', $command_name);
    }

    /**
     * Create a new Notification for a specific user. It's already persisted.
     */
    public static function insertForUser(int $user_id, string $command_name)
    {
        return (new self())->insert([
            'command_name' => $command_name,
            'user_id' => $user_id,
            'send_date' => Carbon::now(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

}
