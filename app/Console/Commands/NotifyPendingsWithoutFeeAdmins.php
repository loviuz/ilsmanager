<?php

namespace App\Console\Commands;

use App\Config;
use App\Console\Commands\NotifyPendingsWithoutFee;
use App\User;
use App\Mail\PendingsWithoutFeeAdmins;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class NotifyPendingsWithoutFeeAdmins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:pendings-without-fee-admins';

    /**
     * The console command description.
     *
     * @var string
     */
    public function getDescription(): string
    {
        return sprintf(
            'Ricorda agli amministratori di importare i conti correnti, %d giorni prima che i soci in attesa che non hanno pagato la quota associativa entro %s giorni dalla registrazione ricevano la loro notifica.',
            $this->getAdminAdvantageDays(),
            NotifyPendingsWithoutFee::daysBetweenRegistrationAndFirstFeeBeforeNotifying()
        );
    }

    /**
     * Get the days of advantage that the admins have to work,
     * before the users are notified.
     * This number should be not too small, or the admins
     * do not have enough time to import movements.
     * Also this MUST not be bigger than the number set in:
     *   getDaysBetweenRegistrationAndFirstFeeBeforeNotifying()
     */
    private function getAdminAdvantageDays()
    {
        return 8;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $notification_name = class_basename(__CLASS__);

        $days_ago = NotifyPendingsWithoutFee::daysBetweenRegistrationAndFirstFeeBeforeNotifying();
        $admin_advantage_days = $this->getAdminAdvantageDays();
        $before_this_registration_date = Carbon::now()->subDays($days_ago + $admin_advantage_days);

        // Retrieve users who haven't paid the fee, but not so recently.
        $users = User::pendingWithoutPaidFee()
            ->where('created_at', '<=', $before_this_registration_date)
            ->doesntHaveNotification($notification_name)
            ->orderBy('created_at', 'asc');

        // If there are users to notify
        $usersList = $users->get()->all();
        if (count($usersList) > 0) {
            // Send an email to the association's email with the list of users who need to pay
            Mail::to(Config::getConfig('association_email'))
              ->send(new PendingsWithoutFeeAdmins($usersList, $days_ago));

            // Add each notification event in the notifications table.
            // So admins are not notified again for these.
            DB::beginTransaction();
            foreach ($usersList as $user) {
                $user->insertNotification($notification_name);
            }
            DB::commit();
        }
    }
}
