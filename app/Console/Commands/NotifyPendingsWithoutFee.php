<?php

namespace App\Console\Commands;

use Exception;

use App\Mail\PendingsWithoutFee;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

/**
 * Notify registered users without any fee.
 * This is command is safe to be called multiple times.
 */
class NotifyPendingsWithoutFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:pendings-without-fee';

    /**
     * Get the console command description.
     */
    public function getDescription(): string
    {
        return sprintf(
            'Notifica i soci in attesa che non hanno pagato la quota associativa dopo %s giorni dalla registrazione.',
            self::daysBetweenRegistrationAndFirstFeeBeforeNotifying()
        );
    }

    /**
     * Get the number of days that must pass between the registration date,
     * and the first fee, before the user receives this reminder.
     * You should choose a number that is not too big or member will forget
     * that thay have registered here. Also, not too small, or it can happen
     * that the admins are just too slow and maybe they have not already imported
     * the missing movements and the related fees.
     */
    public static function daysBetweenRegistrationAndFirstFeeBeforeNotifying(): int
    {
        return 35;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $notification_name = class_basename(__CLASS__);

        $days_ago = self::daysBetweenRegistrationAndFirstFeeBeforeNotifying();
        $before_this_registration_date = Carbon::now()->subDays($days_ago);

        // Iterate through the retrieved users that have not received this notification.
        // Retrieve users who haven't paid the fee, but not so recently.
        $users = User::pendingWithoutPaidFee()
            ->where('created_at', '<=', $before_this_registration_date)
            ->doesntHaveNotification($notification_name);

        foreach ($users->get() as $user) {

            // Write to the original email, not to the custom one,
            // because the custom email may be not activated yet.
            // Do not crash other notifications if we have a problem on this specific user.
            $sent = false;
            try {
                $user->sendMailable(new PendingsWithoutFee($user));
                $sent = true;
            } catch(Exception $e) {
                $this->error(
                  sprintf("[%s] Cannot send email to user so we skip it to retry on next call. User %d[%s]",
                    __CLASS__,
                    $user->id,
                    $user->username));
            }

            if ($sent) {
                // Log the notification in the database so we don't do the same again on next run.
                $user->insertNotification($notification_name);

                // Log notification sent message in console.
                $this->info(sprintf(
                  "[%s] Sent payment reminder to user %d[%s] because user registered at %s that is more than %d days ago",
                  __CLASS__,
                  $user->id,
                  $user->username,
                  $user->created_at,
                  $days_ago));
             }
        }
    }
}
