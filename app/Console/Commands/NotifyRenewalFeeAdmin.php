<?php

namespace App\Console\Commands;

use App\Config;
use App\Mail\RenewalReminderAdmin;
use App\User;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class NotifyRenewalFeeAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:renewal-fee-admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invia una notifica agli amministratori per ricordarsi di importare le nuove quote associative di questo anno appena cominciato';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        Mail::to(Config::getConfig('association_email'))->send(new RenewalReminderAdmin());
    }
}
