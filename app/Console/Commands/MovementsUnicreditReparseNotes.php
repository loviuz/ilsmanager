<?php

namespace App\Console\Commands;

use App\Actions\MovementUnicreditRefreshParsingNotes;
use App\Movement;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class MovementsUnicreditReparseNotes extends Command
{
    protected $signature = 'movements:unicredit-reparse-notes';
    protected $description = 'Forza una nuova lettura delle spese bancarie e degli identificativi dalle causali dei movimenti Unicredit, dove non è rischioso farlo / dove non è stato già fatto. Normalmente non serve lanciare questo comando perché viene già fatto in fase di import.';

    public function handle()
    {
        // Stats
        $all = 0;
        $toucheds = 0;
        $batches = 0;

        Movement::query()
            ->unicredit()
            ->orderBy('id')
            // Micro-optimization: select all movements in reasonable batches.
            ->chunk(100, function (Collection $movements) use (&$all, &$toucheds, &$batches) {
                Log::debug(__CLASS__ . " processing batch: {$batches}: candidates found so far: {$all} toucheds so far: {$toucheds}");
                foreach ($movements as $movement) {
                    $action = new MovementUnicreditRefreshParsingNotes($movement);
                    $action->handle();
                    if ($action->getHasChanges()) {
                        $action->save();
                        $toucheds++;
                    }
                    $all++;
                }
                $batches++;
            });

        Log::info(__CLASS__ . " concluded updating {$toucheds} Movements over {$all} candidates");

        return 0;
    }
}
