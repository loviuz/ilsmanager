<?php

namespace App\Console\Commands;

use Exception;

use App\Mail\RenewalReminder;
use App\User;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class NotifyRenewalFee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:renewal-fee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invia una notifica di rinnovo quota ai soci attivi senza la quota dell\'anno corrente';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        // Ottieni l'anno corrente e l'anno precedente
        $currentYear = date('Y');
        $previousYear = $currentYear - 1;

        // Recupera gli utenti attivi con l'ultima quota pagata nell'anno precedente
        $users = User::active()
            ->hasNotFee($currentYear)
            ->hasFee($previousYear)
            ->get();

        foreach ($users as $user) {
            // In case of specific email delivery issue, don't crash other notifications.
            try {
                $user->sendMailable(new RenewalReminder($user));
            } catch(Exception $e) {
                report($e);
            }
        }
    }
}
