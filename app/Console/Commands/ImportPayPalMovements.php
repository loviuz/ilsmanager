<?php

namespace App\Console\Commands;

use App\Account;
use App\AccountRow;
use App\Movement;
use Illuminate\Console\Command;

use App\User;
use Illuminate\Support\Facades\DB;
use PayPalAPIMovementsReader;
use App\Actions\ImportPayPalMovements as ActionsImportPayPalMovements;

class ImportPayPalMovements extends Command
{
    protected $signature = 'paypal:import';
    protected $description = 'importa movimenti PayPal';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $action = new ActionsImportPayPalMovements();
        $action->handle();
        return 0;
    }
}
