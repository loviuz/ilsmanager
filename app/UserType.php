<?php

namespace App;

use Exception;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * User type
 * Traditionally, this is just 'regular' (normal human being) VS 'association'.
 * @TODO: make an entity
 */
class UserType extends Model {

    const DEFAULT_MEMBER_IDENTIFIER = 'regular';

    /**
     * Get all the well-known user types.
     * @return Collection
     */
    public static function defaults()
    {
        static $cache;
        if (!$cache) {
            $cache = new Collection();
            $type = new self;
            $type->identifier = 'regular';
            $type->label = 'Ordinario';
            $type->member = true;
            $cache->add($type);

            $type = new self;
            $type->identifier = 'association';
            $type->label = 'Associazione';
            $type->member = true;
            $cache->add($type);

            $type = new self;
            $type->identifier = 'guest';
            $type->label = 'Ospite';
            $type->member = false;
            $cache->add($type);
        }

        return $cache;
    }

    /**
     * Get a UserType from its identifier.
     * @param string $identifier Example 'regular' or 'association' or 'guest', etc.
     * @return UserType
     */
    public static function find_by_identifier_or_fail($identifier)
    {
        $all = self::defaults_by_identifier();
        return $all[$identifier] ?? throw new Exception("Il tipo di utente '{$identifier}' non è fra quelli riconosciuti");
    }

    /**
     * Get all the well-known user type identifiers.
     * @return array
     */
    public static function defaults_by_identifier()
    {
        static $cache;
        if (!$cache) {
            $cache = self::defaults()->keyBy('identifier');
        }
        return $cache;
    }

}
