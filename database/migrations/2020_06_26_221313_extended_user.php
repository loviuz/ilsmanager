<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtendedUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone')->nullable();

            $table->string('address_zip')->nullable();

            $table->string('shipping_name')->nullable();
            $table->string('shipping_street')->nullable();
            $table->string('shipping_place')->nullable();
            $table->string('shipping_zip')->nullable();
            $table->string('shipping_prov')->nullable();

            $table->string('size')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone');

            $table->dropColumn('address_zip');

            $table->dropColumn('shipping_name');
            $table->dropColumn('shipping_street');
            $table->dropColumn('shipping_place');
            $table->dropColumn('shipping_zip');
            $table->dropColumn('shipping_prov');

            $table->dropColumn('size');
        });
    }
}
