<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gadget_request_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('uid')->unique();
            $table->string('label');
            $table->string('description')->nullable();
            $table->timestamps();
        });

        DB::table('gadget_request_statuses')->insert([
            ['uid' => 'pending', 'label' => 'Richiesta in attesa di essere verificata', 'description' => 'La richiesta è in attesa di essere verificata. Chi spedisce potrebbe ri-contattarti per chiarire alcuni dubbi.'],
            ['uid' => 'valid', 'label' => 'Richiesta accolta', 'description' => 'La richiesta è stata accolta'],
            ['uid' => 'preparing', 'label' => 'In preparazione', 'description' => 'La richiesta è in fase di preparazione'],
            ['uid' => 'shipped', 'label' => 'Spedita', 'description' => 'La richiesta è stata spedita'],
            ['uid' => 'cancelled', 'label' => 'Annullata', 'description' => 'La richiesta è stata annullata'],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gadget_request_statuses');
    }
};
