<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('surname')->nullable()->change();
            if (Schema::hasColumn('users', 'type')) {
                $table->enum('type', ['regular', 'association', 'guest'])->change();
            }
            if (Schema::hasColumn('users', 'status')) {
                $table->enum('status', ['pending', 'active', 'suspended', 'expelled', 'dropped', 'limited'])->default('pending')->change();
            };
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable(false)->change();
            $table->string('surname')->nullable(false)->change();
            if (Schema::hasColumn('users', 'type')) {
                $table->enum('type', ['regular', 'association'])->change();
            }
            if (Schema::hasColumn('users', 'status')) {
                $table->enum('status', ['pending', 'active', 'suspended', 'expelled', 'dropped'])->default('pending')->change();
            }
        });
    }
};
