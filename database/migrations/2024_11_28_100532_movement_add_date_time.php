<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Upgrade the column "date" from type "DATE" to "DATETIME".
        Schema::table('movements', function (Blueprint $table) {
            $table->datetime('date')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Downgrade the column "date" from type "DATETIM" to "DATE".
        Schema::table('movements', function (Blueprint $table) {
            $table->date('date')->nullable(false)->change();
        });
    }
};
