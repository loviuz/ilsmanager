<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRefundStates extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('refunds', function (Blueprint $table) {
            $table->integer('refunded')->default(0)->change();
            $table->text('admin_notes')->nullable()->after('notes');
        });

        // Edit values of table refunds
        DB::table('refunds')->update(['refunded' => 2], ['refunded' => 1]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Edit values of table refunds
        DB::table('refunds')->update(['refunded' => 1], ['refunded' => 2]);

        Schema::table('refunds', function (Blueprint $table) {
            $table->tinyInteger('refunded')->default(0)->change();
            $table->dropColumn('admin_notes');
        });
    }
};
