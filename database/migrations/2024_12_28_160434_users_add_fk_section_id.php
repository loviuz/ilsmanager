<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // From:
        //    `section_id` int(10) unsigned NOT NULL DEFAULT 0
        // To:
        //    `section_id` int(10) unsigned DEFAULT NULL
        Schema::table('users', function (Blueprint $table) {
            $table->integer('section_id')->unsigned()->nullable(true)->default(null)->change();
        });

        // Promote 0 to NULL, otherwise foreign key makes no sense.
        DB::table('users')
            ->where('section_id', 0)
            ->update([
                'section_id' => null,
            ]);

        // Add foreign key. This implicitly also create indexes.
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Drop foreign keys (note: this keeps related indexes).
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['section_id']);
        });

        // Drop also related indexes.
        // Unfortunately, it seems that we need to drop by exact name.... D: D: D:
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('users_section_id_foreign');
        });

        // Revert null to "0" again.
        DB::table('users')
            ->where('section_id', null)
            ->update([
                'section_id' => 0,
            ]);

        // From:
        //    `section_id` int(10) unsigned DEFAULT NULL
        // To:
        //    `section_id` int(10) unsigned NOT NULL DEFAULT 0
        Schema::table('users', function (Blueprint $table) {
            $table->integer('section_id')->unsigned()->nullable(false)->default(0)->change();
        });
    }
};
