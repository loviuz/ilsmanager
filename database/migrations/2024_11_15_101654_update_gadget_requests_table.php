<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('gadget_requests', function (Blueprint $table) {
            $table->string('street')->nullable()->after('province');
            $table->foreignId('status_id')->constrained('gadget_request_statuses')->onDelete('cascade');
            $table->dropColumn('linuxsi_sticker');
            $table->dropColumn('status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('gadget_requests', function (Blueprint $table) {
            $table->dropForeign(['status_id']);
            $table->dropColumn('status_id');
            $table->dropColumn('street');
            $table->boolean('linuxsi_sticker')->default(false);
            $table->enum('status', ['pending', 'valid', 'preparing', 'shipped', 'cancelled'])->default('pending');
        });
    }
};
