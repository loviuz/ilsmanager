<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('notification_id');
            $table->integer('user_id');
            $table->string('command_name');
            $table->timestamp('send_date');
            $table->text('note')->nullable();
            $table->timestamps();

            // In this way it's quick to:
            //   - show all notifications of a single user,
            //   - check if a single command was sent on a single user, finding the oldest/newest
            $table->index(['user_id', 'command_name', 'send_date']);

            // In this way it's quick to:
            //   - drop old notifications
            $table->index(['send_date']);
        });
    }


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notifications');
    }
};
