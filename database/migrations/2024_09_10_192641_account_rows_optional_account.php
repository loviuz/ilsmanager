<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Drop "NOT NULL".
        Schema::table('account_rows', function (Blueprint $table) {
            $table->integer('account_id')->unsigned()->nullable(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Restore "NOT NULL".
        Schema::table('account_rows', function (Blueprint $table) {
            $table->integer('account_id')->unsigned()->nullable(false)->change();
        });
    }
};
