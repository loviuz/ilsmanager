<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gadget_request_gadget_type', function (Blueprint $table) {
            $table->id();
            $table->foreignId('gadget_request_id')->constrained('gadget_requests')->onDelete('cascade');
            $table->foreignId('gadget_type_id')->constrained('gadget_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gadget_request_gadget_type');
    }
};
