<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEconomicLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('economic_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('section_id')->unsigned();
            $table->decimal('amount', 10, 2);
            $table->integer('account_row_id')->unsigned();
            $table->text('reason');

            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('economic_logs');
    }
}
