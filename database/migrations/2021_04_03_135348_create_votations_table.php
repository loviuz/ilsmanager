<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('assembly_id')->unsigned();
            $table->text('question');
            $table->string('status')->default('pending');
            $table->integer('max_options')->default(1);
            $table->string('options', 2000)->default('');

            $table->foreign('assembly_id')->references('id')->on('assemblies')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votations');
    }
}
