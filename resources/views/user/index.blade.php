@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\User::class)
        <div class="row">
            <div class="col-md-12">
                <h6>I soci vengono approvati una volta l'anno durante l'assemblea per via dello statuto, l'accettazione delle quote é manuale e avviene periodicamente verificando i versamenti sul conto bancario o PayPal.</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUser">Registra Nuovo</button>
                <div class="modal fade" id="createUser" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registra Socio</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('user.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('user.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if($currentuser->hasRole('admin'))
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <ul class="nav nav-tabs card-header-tabs" id="user-charts" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#age-users" role="tab" aria-controls="age-users" aria-selected="true">Età soci</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link"  href="#prov" role="tab" aria-controls="prov" aria-selected="false">Province dei soci</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sections" role="tab" aria-controls="sections" aria-selected="false">Sezioni locali</a>
                                </li>
                            </ul>
                        </div>

                        @php
                        $year = array();
                        $by_year = array();
                        $prov = array();
                        $by_prov = array();
                        $section_by = array();
                        $section = array();
                        $section_by = array();
                        foreach(App\User::where('status', 'active')->where('type', 'regular')->with('section')->get() as $user) {
                            $_year = date('Y', strtotime($user->birth_date));
                            $year[$_year] = $_year;
                            if (!isset($by_year[$_year])) {
                                $by_year[$_year] = 1;
                            } else {
                                $by_year[$_year]++;
                            }

                            $_prov = $user->birth_prov;
                            if ($user->address_prov) {
                                $_prov = $user->address_prov;
                            }
                            $prov[$_prov] = $_prov;

                            if (!isset($by_prov[$_prov])) {
                                $by_prov[$_prov] = 1;
                            } else {
                                $by_prov[$_prov]++;
                            }

                            $_section = $user->section->city ?? null;
                            if ($_section) {
                                $section[$_section] = $_section;

                                if (!isset($section_by[$_section])) {
                                    $section_by[$_section] = 1;
                                } else {
                                    $section_by[$_section]++;
                                }
                            }
                        }

                        ksort($section_by);
                        ksort($section);
                        ksort($by_prov);
                        ksort($prov);
                        ksort($by_year);
                        ksort($year);

                        @endphp

                        <div class="card-body">
                            <div class="tab-content mt-3">
                                <div class="tab-pane active" id="age-users" role="tabpanel">
                                    <div>
                                        <canvas id="age-chart"></canvas>
                                    </div>
                                    <script>
                                    addEventListener("DOMContentLoaded", (event) => {
                                        const ctx = document.getElementById('age-chart');

                                        new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: @json( array_values( $year ) ),
                                                datasets: [{
                                                    label: '# per anno di nascita',
                                                    data: @json( array_values( $by_year ) ),
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                },
                                                ticks: {
                                                    precision:0
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                </div>
                                <div class="tab-pane" id="prov" role="tabpanel">
                                    <div>
                                        <canvas id="prov-chart"></canvas>
                                    </div>
                                    <script>
                                    addEventListener("DOMContentLoaded", (event) => {
                                        const ctx = document.getElementById('prov-chart');

                                        new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: @json( array_values( $prov ) ),
                                                datasets: [{
                                                    label: '# per provincia',
                                                    data: @json( array_values( $by_prov ) ),
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                },
                                                ticks: {
                                                    precision:0
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                </div>
                                <div class="tab-pane" id="sections" role="tabpanel">
                                    <div>
                                        <canvas id="sections-chart"></canvas>
                                    </div>
                                    <script>
                                    addEventListener("DOMContentLoaded", (event) => {
                                        const ctx = document.getElementById('sections-chart');

                                        new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: @json( array_values( $section ) ),
                                                datasets: [{
                                                    label: '# per sezione',
                                                    data: @json( array_values( $section_by ) ),
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                },
                                                ticks: {
                                                    precision:0
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @endif

    @include('user.list')
</div>
@endsection
