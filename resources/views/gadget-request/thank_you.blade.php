@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Grazie per la tua richiesta!</h1>
        <p>La tua richiesta di gadget è stata inviata con successo. Dovresti aver ricevuto una notifica. Controlla anche lo spam. Grazie!</p>
    </div>
@endsection
