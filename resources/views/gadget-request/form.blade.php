<h2>Richiesta di Gadget</h2>

<div class="form-group row">
    <label for="name" class="col-sm-4 col-form-label">Nome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="name" name="name" value="{{ $object ? $object->name : '' }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="surname" class="col-sm-4 col-form-label">Cognome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="surname" name="surname" value="{{ $object ? $object->surname : '' }}" required>
    </div>
</div>

@if (!Auth::user() || ($object && $object->email))
    <div class="form-group row">
        <label for="gadget-email" class="col-sm-4 col-form-label">Email</label>
        <div class="col-sm-8">
            <input type="email" class="form-control" id="gadget-email" name="email" value="{{ $object ? $object->email : '' }}" required>
        </div>
    </div>
@endif

<div class="form-group row">
    <label for="street" class="col-sm-4 col-form-label">Indirizzo e numbero civico</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="street" name="street" value="{{ $object ? $object->street : '' }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="postal_code" class="col-sm-4 col-form-label">CAP</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="postal_code" name="postal_code" value="{{ $object ? $object->postal_code : '' }}" required pattern="[0-9]{5}">
    </div>
</div>

<div class="form-group row">
    <label for="city" class="col-sm-4 col-form-label">Città</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="city" name="city" value="{{ $object ? $object->city : '' }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="province" class="col-sm-4 col-form-label">Provincia</label>
    <div class="col-sm-8">
        @include('commons.provinces', [
            'name' => 'province',
            'id' => 'province',
            'value' => $object ? $object->province : null,
            'required' => true,
        ])
    </div>
</div>

<div class="form-group row">
    <label for="distribution_location" class="col-sm-4 col-form-label">Luogo di Distribuzione</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" id="distribution_location" name="distribution_location" value="{{ $object ? $object->distribution_location : '' }}" placeholder="Scuola, ufficio, ecc.">
    </div>
</div>

<div class="form-group row">
    <label for="subscribe_newsletter" class="col-sm-4 col-form-label">Iscriviti alla Newsletter</label>
    <div class="col-sm-8">
        <input type="hidden" name="subscribe_newsletter" value="0">
        <input type="checkbox" class="form-control" id="subscribe_newsletter" name="subscribe_newsletter" value="1" {{ $object && $object->subscribe_newsletter ? 'checked' : '' }}>
    </div>
</div>

<div class="form-group row">
    <label for="gadget_type" class="col-sm-4 col-form-label">Tipo di Gadget</label>
    <div class="col-sm-8">
        @foreach($gadgetTypes as $gadgetType)
            <div class="form-check" id="gadget_type">
                <input class="form-check-input" type="checkbox" name="gadget_type[]" value="{{ $gadgetType->id }}" id="gadget_type_{{ $gadgetType->id }}"
                    @checked( isset($object->id) ? $object->gadgetTypes->contains($gadgetType->id) : $gadgetType->is_default )
                >
                <label class="form-check-label" for="gadget_type_{{ $gadgetType->id }}">
                    {{ $gadgetType->name }}
                </label>
            </div>
        @endforeach
    </div>
</div>
