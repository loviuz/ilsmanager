@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createGadgetRequest">Nuova Richiesta Gadget</button></p>
                <div class="modal fade" id="createGadgetRequest" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Nuova Richiesta Gadget</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('gadget-request.store') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="modal-body">
                                    @include('gadget-request.form', ['object' => $default_object])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <form method="GET" action="" class="auto-filter-form">
            <div class="row mt-12">
                <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="status">Stato:</label>
                        @foreach($all_statuses as $status)
                            <div>
                                <input id="status-{{ $status->uid }}" type="checkbox" name="status[]" value="{{ $status->uid }}" @checked(in_array($status->id, $selected_status_ids))>
                                <label for="status-{{ $status->uid }}" class="tip" title="{{ $status->description }}">{{ $status->label }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="my-requests">Richiedente:</label>
                        <select name="my_requests" id="my-requests" class="form-control">
                            <option value="0" @selected($my_requests !== '1')>Chiunque</option>
                            <option value="1" @selected($my_requests === '1')>Me</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>

        <div class="row mt-3">
            <div class="col-md-12">

                @if($currentuser->hasRole(['admin', 'shipper']))
                    <p>In qualità di amministratore puoi vedere tutte le richieste di spedizione. Non diffondere questi dati personali senza l'autorizzazione della persona interessata.</p>
                @else
                    <p>Puoi vedere le tue richieste ed eventuali altre richieste anonimizzate.</p>
                @endif

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Data Richiesta</th>
                        <th>Richiedente</th>
                        <th>Email</th>
                        <th>Gadget</th>
                        <th>Stato</th>
                        <th>Dettagli</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($objects as $object)
                        <tr>
                            <td>{{ $object->id }}</td>
                            <td>{{ $object->created_at }}</td>
                            @can('view', $object)
                            <td><pre>{{ $object->shippingAddressRaw }}</pre>
                                @if ($object->isCurrentUserAuthor())
                                    <br />
                                    <em>(mia richiesta)</em>
                                @endif
                            </td>
                            <td>{{ $object->whateverEmail }}</td>
                            @else
                                    <td><em>omissis</em><br />
                                        <em title="Provincia di {{ $object->province }}">{{ $object->province }}</em>
                                    </td>
                                    <td><em>omissis</em></td>
                            @endcan
                            <td>
                                @foreach($object->gadgetTypes as $gadgetType)
                                    <span class="badge badge-info">{{ $gadgetType->name }}</span>
                                @endforeach
                                @if ($object->gadgetTypes->isEmpty())
                                    <em>nessuno</em>
                                @endif
                            </td>
                            <td>{{ $object->status->label }}</td>
                            <td>
                                <a href="{{ route('gadget-request.edit', $object->id) }}" class="btn {{ auth()->user()->can('update', $object) ? '' : 'disabled' }}"><span class="oi oi-pencil"></span></a>

                                @if ($object->tracking_code)
                                    @can('view', $object)
                                    <a href="{{ $object->postalTrackerURL }}" class="btn" target="_blank" title="Traccia spedizione {{ $object->tracking_code }}"><span class="oi oi-map"></span></a>
                                    @endcan
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if ($multi_tracking_url)
                <div>
                    <p><a href="{{ $multi_tracking_url }}" class="btn" target="_blank"><span class="oi oi-map"></span> Traccia le spedizioni</a></p>
                </div>
                @endif

                {{ $objects->links() }}
            </div>
        </div>
    </div>
@endsection
