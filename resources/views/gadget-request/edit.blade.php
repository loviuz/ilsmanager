@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('gadget-request.update', $object->id) }}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf

                    @include('gadget-request.form', ['object' => $object])

                    @if($object)
                        <h3>Stato attuale</h3>
                        <div class="form-group row">
                            <label for="status_id" class="col-sm-4 col-form-label">Stato</label>
                            <div class="col-sm-8">
                                @can('review', $object)
                                    <select class="form-control" id="status_id" name="status_id">
                                        @foreach(\App\GadgetRequestStatus::all() as $status)
                                            <option value="{{ $status->id }}" @selected($status->id == $object->status_id)>{{ $status->label }}</option>
                                        @endforeach
                                    </select>
                                    <p>Attenzione: cambiando il campo dello stato attuale, l'utenza riceverà una notifica informativa.</p>
                                @else
                                    <input type="text" value="{{ $object->status->label }}" readonly disabled>
                                @endcan
                            </div>
                        </div>

                        <h3>Spedizione</h3>

                        @can('view', $object)
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Indirizzo generato sulla busta</label>
                            <div class="col-sm-8">
                                <div class="card">
                                    <pre>{{ $object->shippingAddressRaw }}</pre>
                                </div>
                            </div>
                        </div>
                        @endcan

                        <div class="form-group row">
                            <label for="status_id" class="col-sm-4 col-form-label">Codice di tracciamento</label>
                            <div class="col-sm-8">
                                @can('review', $object)
                                    <input type="text" name="tracking_code" value="{{ $object->tracking_code }}">
                                @else
                                    <input type="text" value="{{ $object->tracking_code }}" readonly disabled>
                                @endcan
                            </div>
                        </div>
                    @endif

                    <hr>

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Salva</button>
                        </div>
                    </div>
                </form>

                <br>

                <form method="POST" action="{{ route('gadget-request.destroy', $object->id) }}">
                    @method('DELETE')
                    @csrf

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-danger{{ Auth::user()->can('delete', $object ) ? '' : ' disabled' }}">Elimina Richiesta di Gadget</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
