@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Dettagli della tua richiesta di spedizione gadget</h1>

        <div class="alert alert-info" role="alert">
            <p>{{ $gadgetRequest->status->description }} (ultimo aggiornamento: {{ $gadgetRequest->updated_at->diffForHumans() }})</p>
        </div>

        @if ($gadgetRequest->tracking_code)
        <div class="alert alert-info" role="alert">
            <p>
                <a href="{{ $gadgetRequest->postalTrackerURL }}" class="btn" target="_blank"><span class="oi oi-map"></span> Traccia la tua spedizione</a>
                <br />
                <small>Attenzione: questo aprirà una pagina con politiche di protezione dei dati diverse dalla nostra.</small>
            </p>
        </div>
        @endif

        <table class="table table-bordered">
            <tbody>
            <tr>
                <th>Nome</th>
                <td>{{ $gadgetRequest->name }}</td>
            </tr>
            <tr>
                <th>Cognome</th>
                <td>{{ $gadgetRequest->surname }}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>{{ $gadgetRequest->whateverEmail }}</td>
            </tr>
            <tr>
                <th>Via</th>
                <td>{{ $gadgetRequest->street }}</td>
            </tr>
            <tr>
                <th>CAP</th>
                <td>{{ $gadgetRequest->postal_code }}</td>
            </tr>
            <tr>
                <th>Città</th>
                <td>{{ $gadgetRequest->city }}</td>
            </tr>
            <tr>
                <th>Provincia</th>
                <td>{{ $gadgetRequest->province }}</td>
            </tr>
            <tr>
                <th>Iscrizione alla Newsletter</th>
                <td>{{ $gadgetRequest->subscribe_newsletter ? 'Sì' : 'No' }}</td>
            </tr>
            <tr>
                <th>Data di creazione</th>
                <td>{{ $gadgetRequest->created_at }}<br />
                    ({{ $gadgetRequest->created_at->diffForHumans() }})</td>
            </tr>
            <tr>
                <th>Data di ultimo aggiornamento</th>
                <td>{{ $gadgetRequest->updated_at }}<br />
                   ({{ $gadgetRequest->updated_at->diffForHumans() }})</td>
            </tr>
            <tr>
                <th>Gadget Richiesti</th>
                <td>
                    <ul>
                        @foreach ($gadgetRequest->gadgetTypes as $gadgetType)
                            <li>{{ $gadgetType->name }}</li>
                        @endforeach
                    </ul>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
