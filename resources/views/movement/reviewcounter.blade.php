@php
$un_reviewed_movements = App\Movement::where('reviewed', 0)->count();
@endphp
@if($un_reviewed_movements)
    <a href="{{ route('movement.review') }}" class="btn btn-primary float-right">{{ $un_reviewed_movements }} Movimenti da Revisionare</a>
@endif
