<?php
if (!isset($name)){
    $name = 'section[]';
}

if (!isset($select)) {
    $select = null;
}

?>

<select class="form-control" name="{{ $name }}" autocomplete="off" @disabled($disabled ?? false)>
    <option value="" {{ (!$select) ? 'selected' : '' }}>Nessuna</option>
    @foreach(App\Section::orderBy('city', 'asc')->get() as $section)
        <option value="{{ $section->id }}" {{ $select == $section->id ? 'selected' : '' }}>{{ $section->city }} ({{ $section->prov }})</option>
    @endforeach
</select>

