@php
$accounts = $accounts ?? App\Account::whereNull('parent_id')->orderBy('name', 'asc')->with('children')->get() ?? [];
$selected_account_ids = $selected_account_ids ?? request()->input('account', []);
$name = $name ?? null;
@endphp
<div class="form-check">
    @include('account/multiselect-entries', [
        'accounts' => $accounts,
        'selected_account_ids' => $selected_account_ids,
        'name' => $name,
        'deep' => 0,
    ])
</div>
