@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('refund.update', $object->id) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf

                @include('refund.form', ['object' => $object])

                @if($object)
                <div class="form-group row">
                    <label for="section_id" class="col-sm-4 col-form-label">Stato</label>
                    <div class="col-sm-8">
                        @if($currentuser->hasRole('admin'))
                        <select class="form-control" id="refunded" name="refunded">
                            @foreach(\App\RefundStatus::all() as $status)
                                <option value="{{ $status->id }}" @selected($status->id == $object->refunded)>{{ $status->label }}</option>
                            @endforeach
                        </select>
                        @else
                            <!-- TODO: improve this graphically :) -->
                            <input type="text" value="{{ $object->refundStatus->label}}" readonly disabled>
                        @endif
                    </div>
                </div>
                @endif

                <hr>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>

            <br>

            @can('delete', $object)
                <form method="POST" action="{{ route('refund.destroy', $object->id) }}">
                    @method('DELETE')
                    @csrf

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-danger">Elimina Richiesta di Rimborso Spese</button>
                        </div>
                    </div>
                </form>
            @endcan
        </div>
    </div>
</div>
@endsection
