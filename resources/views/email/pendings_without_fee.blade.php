@component('mail::message')

# Ancora un passaggio: quota di iscrizione

Non abbiamo ricevuto la tua quota di iscrizione per associarsi ad {{ App\Config::getConfig('association_name') }}.
Nessun problema! Ecco come fare.

@include('email.commons.how_to_pay_user_fee', [
    'user' => $user,
])

----

Grazie per questo impegno!

Ricordiamo che *solo* versando la quota individuale manterrai la tua utenza e i servizi offerti ai soci,
anche per tutto il {{ $user->getFirstQuotaValidityYear() }}.

@endcomponent
