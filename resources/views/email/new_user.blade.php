@if ($user->section)
<p>
    Nuova richiesta di iscrizione nella sezione locale di <b><a href="{{ route( 'section.edit', $user->section->id ) }}">{{ $user->section->city }}</a></b>.
    Questa è la <b>{{ $user->section->users()->count() }}</b>° persona affiliata:
</p>
@else
<p>
    Nuova richiesta di iscrizione:
</p>
@endif
<ul>
    <li>{{ $user->printable_name }} (<a href="mailto:{{ $user->custom_email }}">{{ $user->custom_email }}</a>)</li>
    <li>{{ $user->address_place }} ({{ $user->address_prov }})</li>
    @if ($to_role === 'admin')
    <li><a href="{{ route('user.edit', $user->id) }}">Vedi utenza {{ $user->username }}</a></li>
    @endif
</ul>

@if ($to_role === 'admin')
<p>
    Ricordati di verificare la quota associativa. Per farlo, <a href="{{ route('movement.index') }}">importa i movimenti</a>. Grazie!
</p>
@endif

@if ($to_role === 'referent')
<p>
    Ricevi questa notifica perché sei referente di {{ $user->section->city }} e potresti voler inviare un benvenuto personalizzato all'utenza.<br />
    Comunque nessuna azione è richiesta: la segreteria verificherà la quota di iscrizione al più presto. Al tuo servizio!
</p>
@endif
