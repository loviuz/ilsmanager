@component('mail::message')

# Importare quote associative (scadenza {{ $action_required_before_date }})

È necessario importare tutte le eventuali nuove quote associative da tutti i conti correnti prima del <b>{{ $action_required_before_date }}</b>.

Infatti, il {{ $action_required_before_date }} partiranno notifiche massive ai soci che erano in regola ma che non hanno ancora rinnovato.

Portati avanti con eventuale lavoro arretrato. Ti consigliamo di re-importare i movimenti anche la sera prima del {{ $action_required_before_date }}.

Non ci saranno altri avvisi. Buon lavoro e grazie per il tuo tempo!

@endcomponent
