@component('mail::message')

# Quota associativa in scadenza</title>

Per continuare a far parte di {{ App\Config::getConfig('association_name') }} ti ricordiamo che è necessario rinnovare la quota associativa anche per quest'anno!

@include('email.commons.how_to_pay_user_fee', [
    'user' => $user,
])

## Come evitare questa notifica

Se hai già un pagamento ricorrente e vuoi evitare questa notifica in futuro, ti consigliamo di spostare quel pagamento ricorrente ai primi giorni di gennaio. Questo forse non salverà l'ambiente ma eviterà una email di troppo.

Grazie per il tuo contributo!

@endcomponent
