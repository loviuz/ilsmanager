<!-- resources/views/email/gadget_request_cancelled.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>La tua richiesta di gadget è stata annullata</title>
</head>
<body>
<h1>La tua richiesta di gadget è stata annullata</h1>
<p>Siamo spiacenti di informarti che non ci è possibile processare la tua richiesta di invio gadget all'indirizzo da te specificato.</p>
<p>Questo può capitare se abbiamo finito il budget per le spedizioni e abbiamo terminato i materiali o comunque se non si prevede di produrne a breve.
   È anche possibile che l'indirizzo da te specificato non fosse comprensibile per il nostro servizio di spedizione.</p>
<p>Siamo spiacenti di averti fatto perdere tempo. Puoi sempre contattarci per chiarimenti. Grazie!</p>
</body>
</html>
