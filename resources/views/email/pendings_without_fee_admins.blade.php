@component('mail::message')

# Importa i movimenti degli utenti in attesa e senza quota associativa

Gentile Segreteria,

l'elenco sottostante evidenzia gli utenti in attesa che si sono iscritti da più di {{$registered_since_more_than_days}} giorni e non hanno ancora nessuna quota associativa registrata.

@foreach ($users as $user)
- [{{ $user->name }}]({{route('user.edit', $user->id )}}) - iscrizione {{ $user->created_at->diffForHumans()}}
@endforeach

Per favore, importate i movimenti corrispondenti al più presto per evitare invii di notifiche automatiche agli utenti stessi.

Grazie per la tua collaborazione e buona giornata!

<{{ route('movement.index') }}>

@endcomponent
