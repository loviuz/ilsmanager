@foreach (App\Bank::all() as $bank)
@if ($bank->isPayPal)

### Come pagare con PayPal

Per pagare con PayPal, usa il seguente link rapido:

<a href="{{$bank->getPayPalFeeURL( [ 'user' => $user ] )}}">Dona ora {{ $user->feeAmount }} EUR con {{ $bank->name }}</a>

Oppure puoi inviare manualmente {{ $user->feeAmount }} EUR</a> sul conto PayPal chiamato <code>{{ $bank->identifier }}</code>.

@else

### Come pagare con bonifico

Per pagare con bonifico, invia {{ $user->feeAmount }} EUR a questo IBAN in {{ $bank->name }}:

<pre>{{ $bank->identifier }}</pre>

Causale consigliata:

<pre>{{ \App\AccountRow::createFeeNote( $user) }}</pre>
@endif
@endforeach

### Come pagare con altri metodi

Al momento non abbiamo altri metodi di pagamento.
Ti preghiamo di usare soltanto i metodi ufficiali già menzionati e che puoi verificare anche sul sito ufficiale.

<{{\App\Config::getConfig('donation_url')}}>

## Domande

La tua quota può richiedere fino a 15 giorni per essere processata. Lo facciamo noi, da volontari, semi-manualmente. Grazie per la pazienza.

Una volta processata, la tua ricevuta sarà visibile dal tuo profilo su [{{config('app.name', 'ILSManager')}}]({{ route('home') }}). Puoi entrare con il tuo nome utente "{{ $user->username }}".

Se hai già pagato ed è trascorso troppo tempo e ancora non risulta la tua quota associativa, contattaci pure! Siamo qui per aiutarti.
