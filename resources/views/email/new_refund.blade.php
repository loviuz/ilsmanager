<p>
    Nuova richiesta di rimborso spese n. {{ $refund->id }}:
</p>
<ul>
    <li>{{ $refund->amount }} €</li>
    <li>{{ $refund->user ? $refund->user->printable_name : 'Utente Ignoto' }}</li>
    <li>{{ $refund->section ? $refund->section->city : 'Nessuna Sezione Locale' }}</li>
    <li>{{ $refund->notes }}</li>
</ul>
<p>
    <a href="{{ route('refund.edit', $refund->id) }}">Vedi Rimborso Spese {{ $refund->id }}</a>
</p>
