<!-- resources/views/email/gadget_request_preparing.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>I tuoi gadget sono in fase di preparazione</title>
</head>
<body>
<h1>I tuoi gadget sono in fase di preparazione!</h1>
<p>Una persona volontaria sta elaborando la tua richiesta in questo momento con i dati di spedizione che avevi fornito:</p>
<ul>
    <li><strong>Nome:</strong> {{ $gadgetRequest->name }}</li>
    <li><strong>Cognome:</strong> {{ $gadgetRequest->surname }}</li>
    <li><strong>Indirizzo:</strong> {{ $gadgetRequest->street }}</li>
    <li><strong>CAP:</strong> {{ $gadgetRequest->postal_code }}</li>
    <li><strong>Città:</strong> {{ $gadgetRequest->city }} ({{ $gadgetRequest->province }})</li>
</ul>
<p><a href="{{ $gadgetRequest->editURL }}">Vedi la tua richiesta #{{ $gadgetRequest->id }}</a></p>
<p>Ti informeremo quando i tuoi gadget saranno spediti. Grazie per l'attesa!</p>
</body>
</html>
