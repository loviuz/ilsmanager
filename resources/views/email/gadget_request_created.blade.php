<!-- resources/views/email/gadget_request_created.blade.php -->
<!DOCTYPE html>
<html>
<head>
    <title>La tua richiesta di gadget è stata ricevuta</title>
</head>
<body>
<h1>La tua richiesta di gadget è stata ricevuta</h1>
<p>Abbiamo ricevuto la tua richiesta di spedizione gadget con questi dettagli:</p>
<ul>
    <li><strong>Nome:</strong> {{ $gadgetRequest->name }}</li>
    <li><strong>Cognome:</strong> {{ $gadgetRequest->surname }}</li>
    <li><strong>Indirizzo:</strong> {{ $gadgetRequest->street }}</li>
    <li><strong>CAP:</strong> {{ $gadgetRequest->postal_code }}</li>
    <li><strong>Città:</strong> {{ $gadgetRequest->city }} ({{ $gadgetRequest->province }})</li>
</ul>
<p><a href="{{ $gadgetRequest->editURL }}">Vedi la tua richiesta #{{ $gadgetRequest->id }}</a></p>
<p>Se abbiamo fondi e materiali disponibili e se il tuo indirizzo di spedizione è chiaro, riceverai ulteriori notifiche.
   Nel frattempo, puoi ricontrollare che i tuoi dettagli di spedizione siano corretti. Grazie per l'attesa!</p>
</body>
</html>
