<p>
    La tua iscrizione a {{ App\Config::getConfig('association_name') }} è stata approvata!
</p>
<p>
    Puoi ora accedere al nostro gestionale interno per consultare lo stato della tua iscrizione, scaricare le ricevute o segnalare la tua partecipazione alle assemblee, all'indirizzo<br>
    <a href="{{ route('login') }}">{{ route('login') }}</a><br>
    usando come username il nickname da te scelto: ({{ $user->username }}).<br>
    Ti consigliamo di effettuare <a href="{{ route('password.request') }}">la procedura di recupero password</a> per impostare la tua nuova password di accesso.
</p>
<p>
    Per eventuali domande o dubbi puoi scrivere all'indirizzo email {{ App\Config::getConfig('association_email') }}
</p>
<p>
    Grazie per la tua adesione a {{ App\Config::getConfig('association_name') }}!
</p>
