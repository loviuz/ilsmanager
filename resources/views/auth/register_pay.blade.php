@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h3>
                Grazie per la tua adesione a {{ App\Config::getConfig('association_name') }}!
            </h3>

            <p>
                Hai già versato la tua quota associativa? Se non l'hai fatto, puoi farlo ora, versando <strong>{{ App\Config::getConfig('regular_annual_fee') }} euro</strong>, ad una delle seguenti coordinate:
            </p>

            @foreach(App\Bank::all() as $bank)
                <p>
                    {{ $bank->name }}
		</p>

                    @if($bank->type == 'paypal')
                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="5ZQMHUALNCJHN">
                            <input type="image" src="https://www.paypalobjects.com/it_IT/IT/i/btn/btn_subscribe_LG.gif" border="0" name="submit" alt="Versamento quota via PayPal">
                            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                        </form>
                    @else

			<p>
                        {{ $bank->identifier }}
			</p>
                    @endif
            @endforeach

            <br>

            <p>
                Ti consigliamo di specificare nella causale il tuo nome e cognome.
            </p>

            <p>
                Non appena il pagamento sarà confermato e registrato (periodicamente i versamenti vengono manualmente revisionati) <strong>riceverai una mail di notifica</strong>. Se non la ricevi entro 15 giorni, mandaci una segnalazione!
            </p>

            <p>
                Per ogni genere di dubbio o domanda sulla tua iscrizione, contatta l'indirizzo mail {{ App\Config::getConfig('association_email') }}.
            </p>
        </div>
    </div>
</div>

@endsection
