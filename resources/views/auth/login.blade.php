@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Login</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="username" class="col-sm-4 col-form-label text-md-right">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Ricordami
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Password dimenticata?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if(App::environment('local') && env('DEV_QUICK_LOGIN_BACKDOOR'))
            <div class="form-row">
                <div class="col">
                    <div class="form-group row mb-0">
                        <div class="col-md-4 offset-md-4">
                            <a class="btn btn-link" href="#">
                                {{ __('Dev') }}
                            </a>
                        </div>
                        <div class="col-md-8 offset-md-4">

                            <a class="btn btn-link" href="{{ route('dev.login.admin') }}">
                                {{ __('Admin') }}
                            </a>
                            <a class="btn btn-link" href="{{ route('dev.login.member') }}">
                                {{ __('Member') }}
                            </a>
                            <a class="btn btn-link" href="{{ route('dev.login.referent') }}">
                                {{ __('Referent') }}
                            </a>
                            <a class="btn btn-link" href="{{ route('dev.login.pending') }}">
                                {{ __('Pending') }}
                            </a>
                            <a class="btn btn-link" href="{{ route('dev.login.suspended') }}">
                                {{ __('Suspended') }}
                            </a>
                            <a class="btn btn-link" href="{{ route('dev.login.expelled') }}">
                                {{ __('Expelled') }}
                            </a>
                            <a class="btn btn-link" href="{{ route('dev.login.dropped') }}">
                                {{ __('Dropped') }}
                            </a>
                            <a class="btn btn-link" href="{{ route('dev.login.association') }}">
                                {{ __('Association') }}
                            </a>
                            <a class="btn btn-link" href="{{ route('dev.login.limited') }}">
                                {{ __('Guest Limited') }}
                            </a>

                        </div>
                    </div>
                </div>
            </div>
            @endif

            <p class="text-center mt-3">
                <a href="{{ route('register') }}">Non sei ancora socio ILS? Iscriviti ed aiutaci ad aiutare la community!</a>
            </p>
        </div>
    </div>
</div>
@endsection
