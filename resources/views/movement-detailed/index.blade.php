@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-3">

            @if ($limit > 0)
                <h5 id="bank-last-movements">Ultimi {{ $limit }} Movimenti Bancari</h5>
            @else
                <h5 id="bank-last-movements">Movimenti Bancari</h5>
            @endif

            <p>Ogni Movimento bancario è dettagliato da una o più Righe Contabili.</p>
                <form method="GET" action="" class="auto-filter-form">
                    <div class="row mt-3">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="year">Anno:</label>
                                <select id="year" name="year" class="form-control">
                                    <option value="-" @selected($year === null)>Tutti</option>
                                    @foreach (array_reverse(range(date('Y')-10, date('Y'))) as $available_year)
                                        <option value="{{ $available_year }}" @selected($year == $available_year)>{{ $available_year }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="bank_id">Banca:</label>
                                <select id="bank_id" name="bank_id" class="form-control">
                                    <option value="">Tutte le Banche</option>
                                    @foreach ($banks as $bank)
                                        <option value="{{ $bank->id }}" @selected($bank->id == $bank_id)>{{ $bank->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="with_start_of_year">Includi bilancio di inizio anno:</label>
                                <input type="checkbox" id="with_start_of_year" name="with_start_of_year" class="form-control" @checked($with_start_of_year)>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label>Conti economici:</label>
                            <div style="max-height: 143px; overflow-y: scroll">
                                @include('account/multiselect', [
                                    'selected_account_ids' => $selected_account_ids,
                                ])
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            @include('commons/orderby', [
                                'availables_order_dir' => $availables_order_dir,
                                'order_dir' => $order_dir,
                            ])
                        </div>
                        <div class="col-md-3">
                            @include('commons/limit', [
                                'limit' => $limit,
                            ])
                        </div>
                    </div>
                </form>
            <p><a href="{{ route('movement-detailed.export.all.csv', [
                'year' => $year,
                'bank_id' => $bank_id,
                'account' => $selected_account_ids,
                'order_dir' => $order_dir,
                'limit' => $limit,
            ]) }}">Esporta in CSV</a></p>
        </div>
    </div>
</div>

<div>
    <div class="row">
        <div class="col-md-10 offset-md-1">

            <table class="table">
                <thead>
                    <tr>
                        <th><!-- Actions --></th>
                        <th colspan="4">Movimenti Bancari</th>
                        <th colspan="7" class="border-left">Movimenti in Dettaglio (Righe Contabili)</th>
                    </tr>
                    <tr>
                        <th>ID<br />Movimento</th>
                        <th>Banca<br />Movimento</th>
                        <th>Data<br />Movimento</th>
                        <th>Causale<br />Originale</th>
                        <th>Importo<br />Bancario</th>
                        <th class="border-left">Dettaglio<br />Importo</th>
                        <th>Account<br />Contabile</th>
                        <th>Causale<br />Riga Contabile</th>
                        <th>Utente<br />&nbsp;</th>
                        <th>Sezione<br />Locale</th>
                        <th>Anno<br />Quota</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($movements_dto as $movement_dto)

                            @php
                            $error = null;
                            try {
                                $movement_dto['movement']->validate();
                            } catch(Exception $e) {
                                $error = $e;
                            }
                            @endphp

                            <tr class="{{ $error ? 'bg-warning' : '' }}">
                                @if ($movement_dto['account_row_first'])
                                    <td><a href="{{ route('movement.edit', $movement_dto['movement']->id) }}" title="Modifica Movimento {{ $movement_dto['movement']->id }} del {{ $movement_dto['movement']->date }}"><span class="oi oi-pencil"></span> {{ $movement_dto['movement']->id }}</a></td>
                                    <td><a href="{{ route('bank.edit', $movement_dto['movement']->bank_id) }}" title="Modifica Banca: {{ $movement_dto['movement']->bank->name }}">{{ $movement_dto['movement']->bank->name }}</a></td>
                                    <td>{{ $movement_dto['movement']->date }}</td>
                                    <td>{{ $movement_dto['movement']->notes }}</td>
                                    <td>{{ $movement_dto['movement']->amount_formatted }}</td>
                                @else
                                    <td><!-- Edit Movement already printed --></td>
                                    <td><abbr title="{{ $movement_dto['movement']->bank->name }}">&hellip;</abbr></td>
                                    <td><abbr title="{{ $movement_dto['movement']->date }}">&hellip;</abbr></td>
                                    <td><abbr title="{{ $movement_dto['movement']->notes }}">&hellip;</abbr></td>
                                    <td class="text-right"><abbr title="{{ $movement_dto['movement']->amount_formatted }}">&hellip;</abbr></td>
                                @endif

                                <td class="border-left text-right {{ $movement_dto['movement']->isAmountMatchingAccountRows() ?: 'bg-danger' }}">
                                    {{ $movement_dto['account_row'] ? $movement_dto['account_row']->amount_formatted : '' }}
                                </td>

                                <td>{{ isset($movement_dto['account_row']->account) ? $movement_dto['account_row']->account->name : '' }}</td>
                                <td>{{ $movement_dto['account_row']           ? $movement_dto['account_row']->notes         : '' }}</td>

                                <td>
                                @if ( $movement_dto['account_row'] && $movement_dto['account_row']->user )
                                <a href="{{ route('user.edit', $movement_dto['account_row']->user->id) }}">{{ $movement_dto['account_row']->user->printableName }}</a>
                                @endif
                                </td>

                                <td>
                                @if ( $movement_dto['account_row'] && $movement_dto['account_row']->section )
                                <a href="{{ route('section.edit', $movement_dto['account_row']->section->id) }}">{{ $movement_dto['account_row']->section->city }}</a>
                                @endif
                                </td>

                                <td>{{ $movement_dto['fee'] ? $movement_dto['fee']->year : '' }}</td>
                            </tr>

                            @if ($error)
                                <tr class="bg-warning">
                                    <td>&nbsp;</td>
                                    <td colspan="10"><b>{{ $e->getMessage() }}</b></td>
                                </tr>
                            @endif

                        @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>TOTALI</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th title="Somma dei Movimenti bancari">{{ $movements_amount_sum }}</th>
                        <th title="Somma dei dettagli delle righe contabili">{{ $account_rows_amount_sum }}</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
            <div class="pagination-wrapper">
                {{ $movements->appends(request()->query())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
