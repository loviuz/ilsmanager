@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\Sponsor::class)
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createSponsor">Registra Nuovo</button>
                <div class="modal fade" id="createSponsor" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registra Sponsor</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('sponsor.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('sponsor.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Scadenza</th>
                        @if($currentuser->hasRole('admin'))
                            <th>Modifica</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $sponsor)
                        @php
                            $class = '';

                            // Arancione se scadrà fra 4 mesi
                            if ($sponsor->expiration < date('Y-m-d', strtotime('+4 months'))) {
                                $class = 'bg-warning';
                            }

                            // Rosso se scaduto
                            if ($sponsor->expiration < date('Y-m-d')) {
                                $class = 'bg-danger';
                            }
                        @endphp
                        <tr class="{{ $class }}">
                            <td>{{ $sponsor->name }}</td>
                            <td>{{ $sponsor->expiration }}</td>

                            @if($currentuser->hasRole('admin'))
                                <td><a href="{{ route('sponsor.edit', $sponsor->id) }}"><span class="oi oi-pencil"></span></a></td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
