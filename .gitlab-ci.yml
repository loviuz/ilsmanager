stages:
  - build-docker-image
  - release-docker-image
  - build
  - lint
  - test

# https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html#use-gitlab-cicd
default:
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

# https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html#use-gitlab-cicd
variables:
  # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  # docker-compose -f compose.gitlab.yaml
  COMPOSE_FILE: compose.gitlab.yaml
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest

#
# Build the Docker image for the "web" container (just PHP and extra stuff)
#
build-docker-test-web-image:
  stage: build-docker-image
  script:
    - docker pull --quiet    $CONTAINER_TEST_IMAGE || true
    - docker build --pull -t $CONTAINER_TEST_IMAGE --cache-from $CONTAINER_TEST_IMAGE .
    - docker push            $CONTAINER_TEST_IMAGE
  rules:
    - when: always

#
# Release the Docker image for the "web" container (just PHP and extra stuff).
# This is the image that will be used by "docker-compose up".
#
release-docker-web-image-on-default-branch:
  stage: release-docker-image
  script:
    - docker pull --quiet $CONTAINER_TEST_IMAGE
    - docker tag          $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push         $CONTAINER_RELEASE_IMAGE
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

#
# This is the first "docker-compose run" that will build the "vendor/" directory.
# This will speedup consequent "docker-compose run(s)" or whatever.
#
configure-and-first-run-for-composer-build:
  stage: build
  needs: ["build-docker-test-web-image"]
  cache:
    key:
      files:
        - composer.lock
    paths:
      - vendor/
  before_script:
      - cp .env.example .env
      - "echo \"ILSMANAGER_DOCKER_IMAGE_WEB=$CONTAINER_TEST_IMAGE\" >> .env"
  script:
      - docker-compose run web echo ok
  artifacts:
    expire_in: 1 month
    paths:
      - vendor/
      - .env
  rules:
    - when: always

#
# Linter on normal atomic commits.
#
codestyle-for-atomic-commit:
  stage: lint
  needs: ["configure-and-first-run-for-composer-build"]
  cache:
    key:
      files:
        - composer.lock
    paths:
      - vendor/
  before_script:
    - apk add git
  script:
    - echo "THIS IS A NORMAL COMMIT"
    - GIT_CHANGED_FILES=$(git diff-tree --no-commit-id --name-only -r "$CI_COMMIT_SHA")
    - echo "Running Duster lint on these files that changed from single commit ${CI_COMMIT_SHA}. Changed files - $GIT_CHANGED_FILES (if you see nothing - nothing will be tested)."
    - test -n "$GIT_CHANGED_FILES" && docker-compose run web duster lint -- $GIT_CHANGED_FILES || echo "No changed files. Test skipped."
  except:
    - merge_requests
    - external_pull_requests

#
# Linter on merge requests
#
codestyle-for-merge-request:
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
    MY_COMMIT_RANGE: "${CI_MERGE_REQUEST_DIFF_BASE_SHA}..${CI_COMMIT_SHA}"
  stage: lint
  needs: ["configure-and-first-run-for-composer-build"]
  cache:
    key:
      files:
        - composer.lock
    paths:
      - vendor/
  before_script:
    - apk add git
  script:
    - echo "THIS IS A MERGE REQUEST"
    - GIT_CHANGED_FILES=$(git diff-tree --no-commit-id --name-only -r "$MY_COMMIT_RANGE")
    - echo "Running Duster lint on these files that changed from commit range $MY_COMMIT_RANGE. Changed files - $GIT_CHANGED_FILES (if you see nothing, nothing will be executed)"
    - test -n "$GIT_CHANGED_FILES" && docker-compose run web duster lint -- $GIT_CHANGED_FILES || echo "No changed files. Test skipped."
  only:
    - merge_requests
    - external_pull_requests

#
# Artisan Tests
# https://laravel.com/docs/11.x/testing
#
artisan-test:
  stage: test
  needs: ["configure-and-first-run-for-composer-build"]
  cache:
    key:
      files:
        - composer.lock
    paths:
      - vendor/
  script:
      - docker-compose run web php artisan test
  rules:
    - when: always

# phpunit:
#   stage: test
#   dependencies:
#     - composer
#   script:
#     - phpunit --coverage-text --colors=never

#release-production:
#  stage: release
#  script:
#    # Note that ilsmanager.linux.it is very picky about people who can connect to.
#    # So the servizi.linux.it is probably a good solution as "proxy jump server".
#    - ssh -J ilsprodjump-nonsense-jumper@servizi.linux.it root@ilsmanager.linux.it /root/deploy.sh
#    # only deploy if it's in the ItalianLinuxSociety namespace
#    - if: $CI_PROJECT_NAMESPACE != "ItalianLinuxSociety"
#      when: never
#  only:
#    - master
#    - tags
