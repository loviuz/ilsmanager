<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('emails', 'ApiController@emails')->name('api.emails');
Route::get('websites', 'ApiController@websites')->name('api.websites');
Route::middleware('auth:api')->get('/profile', [UserController::class, 'profile']);
